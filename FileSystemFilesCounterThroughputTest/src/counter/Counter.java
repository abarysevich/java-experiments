package counter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Nice case for fork/join test
/**
 * Serialized execution:
 * 20574222 - normal 
 * 9519 - broken IO Files
 * 2346605 - execution time 
 * @author abarysevich
 * 
 * 
 * 
 * Count of threads is the dame as root files (27)
 * 20591761 - normal files
 * 0 - broken IO Files
 * 2308949 - execution time
 */
public class Counter {
	private static List<Integer> filesCountResultsContainer = 
			Collections.synchronizedList(new ArrayList<Integer>()); 
	private static List<Integer> brokenFilesCountResultsContainer =  
			Collections.synchronizedList(new ArrayList<Integer>());

	
	public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
		File rootDir = new File("/");
		File[] children = rootDir.listFiles();
		CyclicBarrier barrier = new CyclicBarrier(children.length + 1, new Runnable() {
			@Override
			public void run() {
				int filesCount = 0;
				for (Integer filesFromOneThread: filesCountResultsContainer) {
					filesCount += filesFromOneThread;
				}
				
				int brokenFilesCount = 0;
				for (Integer filesFromOneThread: brokenFilesCountResultsContainer) {
					brokenFilesCount += filesFromOneThread;
				}
				
				System.out.println(filesCount);
				System.out.println(brokenFilesCount);
			}
			
		});
		
		ExecutorService executor = Executors.newCachedThreadPool();
		for (File file: rootDir.listFiles()) {
			executor.execute(new CounterThread(file, barrier));
		}
		
		long startTime = System.currentTimeMillis();
		// Threads start after recieving this point:
		barrier.await();
		// All threads finished after this point:
		barrier.await();
		long endTime = System.currentTimeMillis();
		System.out.println(String.format("Used time is %d", endTime - startTime));
		executor.shutdown();
	}

	private static class CounterThread implements Runnable {
		private File rootDir;
		private CyclicBarrier barrier;
		private int filesCount;
		private int brokenFilesCount;
		
		public CounterThread(File rootDir, CyclicBarrier barrier) {
			this.rootDir = rootDir;
			this.barrier = barrier;
		}
		
		public void run() {
			try {
				barrier.await();
				countFiles(rootDir);
				filesCountResultsContainer.add(filesCount);
				brokenFilesCountResultsContainer.add(brokenFilesCount);
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
		
		private void countFiles(File file) {
			if (file.isDirectory()) {
				File[] children = file.listFiles();
				if (children == null) {
					brokenFilesCount++;
					return;
				}
				for (File childFile: children) {
					countFiles(childFile);
				}
		    } else {
		    	filesCount++;
		    }
		}
	}
	
}
