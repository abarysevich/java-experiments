package counter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

// Nice case for fork/join test

/**
 * Thread pool is unbounded:
 * 
 * Thread count is the same as CPU-s:
 * Thread number 0 has counted 65766 files
 * Thread number 1 has counted 20532614 files
 * Used time is 2164904
 * Result files count is 20598380
 * 
 * Thread count is the same as CPU-s + 1:
 * Thread number 1 has counted 62726 files
 * Thread number 0 has counted 65365 files
 * Thread number 2 has counted 20470676 files
 * Used time is 2155814
 * Result files count is 20598767
 * 
 * Thread count is the same as CPU-s + 20:
 * Thread number 1 has counted 1 files
 * Thread number 2 has counted 1 files
 * Thread number 3 has counted 1 files
 * Thread number 4 has counted 0 files
 * Thread number 5 has counted 1 files
 * Thread number 7 has counted 0 files
 * Thread number 6 has counted 1 files
 * Thread number 9 has counted 0 files
 * Thread number 11 has counted 0 files
 * Thread number 14 has counted 0 files
 * Thread number 16 has counted 1 files
 * Thread number 17 has counted 0 files
 * Thread number 18 has counted 0 files
 * Thread number 10 has counted 37 files
 * Thread number 20 has counted 60 files
 * Thread number 13 has counted 268 files
 * Thread number 12 has counted 364 files
 * Thread number 19 has counted 1239 files
 * Thread number 15 has counted 62057 files
 * Thread number 8 has counted 65359 files
 * Thread number 21 has counted 20469777 files
 * Used time is 2137900
 * Result files count is 20599168
 * 
 * Thread pool is bounded with CPU-s count threads:
 * 
 * Thread count is the same as CPU-s:
 * Thread number 0 has counted 65765 files
 * Thread number 1 has counted 20533919 files
 * Used time is 2177844
 * Result files count is 20599684
 */
public class ThreadsAsCoresCounter {
	private static AtomicInteger resultFiles = new AtomicInteger(0); 
	
	public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
		File rootDir = new File("/");
		int processors = Runtime.getRuntime().availableProcessors();
		List<List<File>> rootFilesSublists = getRootFilesSublist(rootDir, processors);
		CyclicBarrier barrier = new CyclicBarrier(rootFilesSublists.size() + 1);
		ExecutorService executor = Executors.newFixedThreadPool(processors);
		
		for (int threadNumber = 0; threadNumber < rootFilesSublists.size(); threadNumber++) {
			String threadName = String.format("Thread number %d", threadNumber);
			executor.execute(
				new CounterThread(rootFilesSublists.get(threadNumber), barrier, threadName));
		}

		long startTime = System.currentTimeMillis();
		// Threads start after recieving this point:
		barrier.await();
		// All threads finished after this point:
		barrier.await();
		long endTime = System.currentTimeMillis();
		System.out.println(String.format("Used time is %d", endTime - startTime));
		System.out.println(String.format("Result files count is %d", resultFiles.get()));
		executor.shutdown();
	}
	
	private static List<List<File>> getRootFilesSublist(File rootDir, int sublistsCount) {
		List<List<File>> rootFilesChunks = new ArrayList<>();
		List<File> children = Arrays.asList(rootDir.listFiles());
		
		
		int sublistLength = children.size() / sublistsCount;
		int begin = 0;
		int end = sublistLength;
		for(int i = 1; i <= sublistsCount; i++) {
			if (i==sublistsCount) {
				rootFilesChunks.add(children.subList(begin, children.size()));
				break;
			}
			rootFilesChunks.add(children.subList(begin, end));
			begin = end;
			end += sublistLength;
		}
		return rootFilesChunks;
	}

	private static class CounterThread extends Thread {
		private List<File> rootDirs;
		private CyclicBarrier barrier;
		private int filesCount;
		
		public CounterThread(List<File> rootDirs, CyclicBarrier barrier, String threadName) {
			this.rootDirs = rootDirs;
			this.barrier = barrier;
			this.setName(threadName);
		}
		
		public void run() {
			try {
				barrier.await();
				for (File rootDir: rootDirs) {
					countFiles(rootDir);
				}
				resultFiles.addAndGet(filesCount);
				System.out.println(String.format("%s has counted %d files", getName(), 
						filesCount));
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
		
		private void countFiles(File file) {
			if (file.isDirectory()) {
				File[] children = file.listFiles();
				if (children == null) {
					filesCount++;
					return;
				}
				for (File childFile: children) {
					countFiles(childFile);
				}
		    } else {
		    	filesCount++;		 
		   	}
		}
	}
}
