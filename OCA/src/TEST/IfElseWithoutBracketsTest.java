package TEST;

public class IfElseWithoutBracketsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean flag = false;
		if (flag)
		   System.out.println("Inside if block");
//		   Only one statement is possible in if without brackets
//		   System.out.println("Inside if block");
		else 
		   System.out.println("Inside else block");
		   System.out.println("Out of if statement");
	}
}
