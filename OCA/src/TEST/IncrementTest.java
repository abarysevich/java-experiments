package TEST;

public class IncrementTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int x = 10;
		int y = x++;
		System.out.println(x +" "+y);
		
		float i = 1.1f;
		
		byte a = 127;
		byte b = 70;
		
//	    two bytes sum is integer despite its actual value!!!
//		byte n = (a + b);
	    
		byte n = (byte)(a + b);
		System.out.println(n);
	
//		It is not compiled if literal value is out of range for current primitive type
//		short shortTest = 32770;
		short shortSecondTest = 32760;

		
		int  c = Integer.MAX_VALUE -1;
		int d = 3;
		int result = c + d;
		System.out.println(result);
	

	}

}
