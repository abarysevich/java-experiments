package TEST;

public class MethodWithConstructorName {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MethodWithConstructorName constructor = new MethodWithConstructorName();
		constructor.MethodWithConstructorName();
	}

	// Can exist without any compiler errors:
	public void MethodWithConstructorName() {
		System.out.println("This is not constructor");
	}
}
