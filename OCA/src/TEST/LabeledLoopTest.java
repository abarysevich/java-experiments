package TEST;

public class LabeledLoopTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean flag = false;
		
		labelOut:
		while(true) {
			labelIn:
			while(true) {
				System.out.println("Inner loop");
				if (flag) {
					continue labelOut;
				} else {
					break labelIn; 
				}
			}
			System.out.println("Outer loop");
		}
	}

}
