package TEST;

import java.io.Closeable;
import java.io.IOException;

public class OverridingResolution {

//	Constructor can not be final!!!
//	public final OverridingResolution() {
//		
//	}
	
	private Closeable close = new Closeable() {
		@Override
		public void close() throws IOException {
			// TODO Auto-generated method stub
		}
	};
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
//		ambigous - doesn't compile
//		new OverridingResolution().sum(1, 2);
		
		new OverridingResolution().sum(1L, 2);	

//		Check this!!!
// Overriding resolution:		
// 1 step: tries to find appropriate signature amid primitive values (can be downcasting)
// 2 step: tries to find appropriate signature amid wrapper values (can't be downcasting)!!!	
// 3 step: tries to find appropriate signature amid varargs values (can be downcasting)	
//		a: autoboxing doesn't work in varags case
		
		new OverridingResolution().sum(2);		
	}

	private void sum(long first, int second) {
		System.out.println("first");	
		
//		local class can not be static!!!
//		static class A{};
		
	}
	
	private void sum(int first, long second) {
		System.out.println("second");
	}
	
	private void sum(short arg) {
		System.out.println("short");
	}
	
//
//	private void sum(Integer arg) {
//		System.out.println("Integer");
//	}

	// Downcasting doesn't work with autoboxing simultaneously during overloading
	// resolution !!! 
	private void sum(Long arg) {
		System.out.println("Integer");
	}
	
	
//	private void sum(long arg) {
//		System.out.println("Integer");
//	}
	
	public boolean equals(Object object) {
		String test = "test";
		System.out.println(test);
		
		super.equals(object);
		
		return false;
	}
	
}
