package TEST;

public class OverridingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	class A {
		public String test() {
			return null;
		}
	}
	
	class B extends A{
		// Should be the same return type in case of overriding - otherwise doesn't compile
		public int test() {
			return 0;
		}
	}
	
}
