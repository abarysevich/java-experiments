
// Possible package name with upper case letters
package TEST;

public class SwitchStringTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("test");
		
		// java.lang package is imported by default!!
		String a = null;
		
		// NullPointer if null value is provided
		switch(a) {
			case "b": System.out.print(true);
//			Works without!!! Just nothing is printed!!
//			default: System.out.println("Yo");
		}
	}

}
