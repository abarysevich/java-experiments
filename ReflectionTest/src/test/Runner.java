package test;

import java.lang.reflect.Field;

public class Runner {

	/**
	 * @param args
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException 
	{
		TestClass test1 = new TestClass();
		TestClass test2 = new TestClass();
		
		@SuppressWarnings("unchecked")
		Class<TestClass> clazz = (Class<TestClass>) test1.getClass();
		
		Field testField = clazz.getDeclaredField("testField");
		testField.setAccessible(true);
		testField.setInt(test1, 0);
		testField.getInt(test1);
		testField.setAccessible(false);
	
		
		System.out.println(test1.getTestField());
		System.out.println(test2.getTestField());
	}

}
