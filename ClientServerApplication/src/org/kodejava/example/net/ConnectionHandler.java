package org.kodejava.example.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

class ConnectionHandler implements Runnable 
{
	
    private Socket m_socket;
 
    public ConnectionHandler(Socket socket) 
    {
        this.m_socket = socket;
        Thread t = new Thread(this);
        t.start();
    }
 
   public void run() 
   {
        try
        {
	       //
	       // Read a message sent by client application
	       //
           ObjectInputStream ois = new ObjectInputStream(m_socket.getInputStream());
           String message = (String) ois.readObject();
           System.out.println("Message Received: " + message);

           //
           // Send a response information to the client application
           //
           ObjectOutputStream oos = new ObjectOutputStream(m_socket.getOutputStream());
           oos.writeObject("Hi...");
 
           ois.close();
           oos.close();
           m_socket.close();

           System.out.println("Waiting for client message...");
       } 
       catch (IOException e) 
       {
            e.printStackTrace();
       } 
       catch (ClassNotFoundException e)
       {
           e.printStackTrace();
       }
    }
   
}
