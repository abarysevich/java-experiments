package test;

public class JVMParamsWatcher
{
	public static void main(String[] args) 
	{
		String javaLibName = "java.library.path";
		String nativePath = System.getProperty(javaLibName);
		System.out.println(String.format("The way for searching native dll's is: %s", nativePath));
		
		System.setProperty(javaLibName, "test");
		String nativePathEdit = System.getProperty(javaLibName);
		System.out.println(String.format("The way for searching native dll's is: %s", nativePathEdit));
	}
}
