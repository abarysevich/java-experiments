<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
		<xsl:output method="html"/>

 	<xsl:template match="/">
		<h1>
			<xsl:value-of select="shiporder/orderperson"/> 
		</h1>
	</xsl:template>
 
 		
	<xsl:template name="shiporder">
		<h1>
			<xsl:value-of select="shipto/name"/> 
		</h1>
	</xsl:template>
	
</xsl:stylesheet>