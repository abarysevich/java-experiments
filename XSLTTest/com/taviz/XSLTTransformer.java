package com.taviz;

import java.io.File;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XSLTTransformer
{
	
	public static void main(String[] params) throws Exception
	{
		File stylesheetFile = new File("stylesheet.xsl");
		File testFile = new File("test.xml");
		transform(stylesheetFile, testFile);
	}
	
	/**
	 * 
	 * @param sourse
	 */
	private static void transform(File styleSheet, File fileForTransformation) 
																throws Exception
	{
		StreamSource styleSource = new StreamSource(styleSheet);
		Transformer trans = TransformerFactory.newInstance().newTransformer(styleSource);
		Source inputFile = new StreamSource(fileForTransformation);
		Result result = new StreamResult(new File("transformationResult.html"));
		trans.transform(inputFile, result);
	}
	
}
