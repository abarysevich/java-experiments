package com.borex.pojos;

import com.google.auto.value.AutoValue;
import org.codehaus.jackson.annotate.JsonCreator;

import java.util.Date;

@AutoValue
public abstract class Event {

    @JsonCreator
    public static Event createEvent(EventType eventType,
                                    Date eventDate,
                                    String description) {
        return new AutoValue_Event(eventType, eventDate, description);
    }

    abstract EventType type();

    abstract Date date();

    //  TODO make it nullable @Nullable
    abstract String description();
}
