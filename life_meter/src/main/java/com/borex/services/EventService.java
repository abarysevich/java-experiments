package com.borex.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.borex.dao.EventDao;
import com.borex.pojos.Event;

@Service
public class EventService {
    // TODO add logger.

    private EventDao dao;

    @Autowired
    public EventService(EventDao dao) {
        this.dao = dao;
    }

    public void addEvent(Event event) {
        System.out.println(String.format("New event was added: %s.", event));
    }
}
