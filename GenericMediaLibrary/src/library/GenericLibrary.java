package library;

import java.util.ArrayList;

import media.Media;

public class GenericLibrary <T extends Media> 
{
	
	ArrayList<T> library;
    
	public GenericLibrary()
	{
		library = new ArrayList<T>();
	}
	
	public void addMedia(T media)
	{
		library.add(media);
	}
	
	public T getMedia()
	{
		return library.iterator().next();
	}
	
}
