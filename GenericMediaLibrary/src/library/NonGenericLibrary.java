package library;

import java.util.ArrayList;

import media.Media;



public class NonGenericLibrary
{
	
	private ArrayList library;
	
	public NonGenericLibrary()
	{
		library = new ArrayList();
	}
	
	public void addMedia(Media media)
	{
		library.add(media);
	}
	
	public Media getMedia()
	{
		return (Media)library.iterator().next();
	}

}
