package priorityqueue;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
//		<p>The <em>head</em> of this queue is the <em>least</em> element
//		 * with respect to the specified ordering.  If multiple elements are
//		 * tied for least value, the head is one of those elements -- ties are
//		 * broken arbitrarily.
		Queue<String> priorityQueue = new PriorityQueue<>();
		priorityQueue.add("one");
		priorityQueue.add("two");
		
		
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		
		System.out.println("one".compareTo("two"));
	}

}
