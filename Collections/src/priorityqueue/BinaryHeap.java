package priorityqueue;

public class BinaryHeap {
	private int length;
	private int[] heap;

	public BinaryHeap(int[] elements) {
		this.length = elements.length;
		this.heap = new int[this.length*2];
		System.arraycopy(elements, 0, heap, 0, elements.length);
		heapify();
	}
	
	public int getMax() {
		if (isEmpty()) {
			throw new IllegalStateException();
		}
		return heap[0];
	}
	
	public int exctractMax() {
		if (isEmpty()) {
			throw new IllegalStateException();
		}
		int max = heap[0];
		heap[0] = heap[length-1];
		length--;
		sink(0);
		return max;
	}
	
	public void addElement(int newElement) {
		heap[length] = newElement;
		swim(length);
		this.length++;
		// TODO resize if required!!!
	}
	
	public boolean isEmpty() {
		return this.length == 0;
	}
	
	private void heapify() {
		for(int i=1; i<length;i++)
			swim(i);
	}
	
	private void swim(int elementIndex) {
		if (elementIndex == 0) 
			return;
		int parentIndex = elementIndex%2;
		if (heap[parentIndex] < heap[elementIndex]) {
			exchange(parentIndex, elementIndex);
			swim(parentIndex);
		}
	}
	
	private void sink(int heapIndex) {
		int firstChildIndex = heapIndex*2;
		if (firstChildIndex >= length) {
			return;
		}
		int secondChildIndex = firstChildIndex+1;
		int largerChildIndex = -1;
		if (secondChildIndex >=length) {
			largerChildIndex = firstChildIndex;
		} else {
			largerChildIndex = 
				getLargerElementIndex(firstChildIndex, secondChildIndex);
		}
		if (heap[largerChildIndex] > heap[heapIndex]) {
			exchange(largerChildIndex, heapIndex);
			sink(largerChildIndex);
		}
	}
	
	private int getLargerElementIndex(int firstIndex, int secondIndex) {
		if (heap[firstIndex] >= heap[secondIndex]) {
			return firstIndex;
		} else {
			return secondIndex;
		}
	}

	private void exchange(int first, int second) {
		int temp = heap[first];
		heap[first] = heap[second];
		heap[second] = temp;
	}
}
