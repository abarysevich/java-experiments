package test;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.SetMultimap;

public class MultimapsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String key = "First";
		Long value = Long.valueOf(1L);
		ListMultimap<String, Long> listMultimap = ArrayListMultimap.create();
		listMultimap.put(key, value);
		listMultimap.put(key, value);

		// List is used
		List<Long> listValues = listMultimap.get(key);
		System.out.println(listMultimap.get(key));
	
		SetMultimap<String, Long> setMultimap = HashMultimap.create();
		setMultimap.put(key, value);
		setMultimap.put(key, value);
		
		// Set is used
		Set<Long> setValues = setMultimap.get(key);
		setValues.add(2L);
		System.out.println(setMultimap.get(key));
	}
}
