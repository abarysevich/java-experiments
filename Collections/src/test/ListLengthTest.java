package test;

import java.util.ArrayList;

public class ListLengthTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Object> list = new ArrayList<>();
		list.add(new Object());
		list.add(new Object());
		list.add(new Object());
		System.out.println(String.format("List length before deleting element %d", list.size()));
		// List.size() returns counts of elements keeped in this list
		list.remove(1);
		System.out.println(String.format("List length after deleting element %d", list.size()));
		// List keeps its elements in in the aggregated array. This array actual size is trimed to
		// size of keeped elements. Array initial size if ArrayList was instantiated with default
		// constructor is 10. After executing method below it size becames same as keeped elements
		// count (in this particular case 2).
		list.trimToSize();
		System.out.println(String.format("List length after trim to size call %d", list.size()));
	}

}
