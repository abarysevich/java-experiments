package test;

import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

public class NavigableSetTest {


	public static void main(String[] args) {
		
		NavigableSet<String> navigableSet = new TreeSet<>();
		navigableSet.add("C");
		navigableSet.add("c");
		
		navigableSet.add("E");
		navigableSet.add("e");
		
		navigableSet.add("F");
		navigableSet.add("f");
		
//		NavigableSet<Integer> navigableSet = new TreeSet<>();
//		navigableSet.add(5);
//		navigableSet.add(10);
//		navigableSet.add(15);

//		Normal order : lesser the first
//		'A' is lesser than 'a' !!!!!!!
		System.out.println(navigableSet);
		
//		Revers order : bigger the first
		System.out.println(navigableSet.descendingSet());

//		The lowest element
		System.out.println(navigableSet.first());
		
//		The biggest element
		System.out.println(navigableSet.last());
		
//		Returns the least element greater than or equal to the given element
		
//		System.out.println(navigableSet.ceiling("a"));
//		If there is no elements bigger than given returns null
		System.out.println(navigableSet.ceiling("c"));

//		returns the least element strictly greater than inut element
		System.out.println(navigableSet.higher("c"));
		
//		Returns the greatest element less than or equal to the given element
//		System.out.println(navigableSet.floor("a"));
//      If there is no elements less than given, returns null 		
		System.out.println(navigableSet.floor("E"));
		
//		Returns strictly biggest element that is lesser than input element 
		System.out.println(navigableSet.lower("E"));
		
//		Returns the subset of elements from this set of elements which are greater
//		than given element
//		System.out.println(navigableSet.tailSet("a"));

//		Returns the subset of elements from this set of elements which are lower
//		than given element
//		System.out.println(navigableSet.headSet("a"));
		
		// Result is also SortedSet - from element is included - to element not 
		// included like with subString() 
		System.out.println(navigableSet.subSet("E", "e"));
		
		TreeSet<String> treeSet = new TreeSet<>();
		treeSet.add("a");
		treeSet.add("ab");
		treeSet.add("abc");
		
		Set<String> subSet = treeSet.tailSet("ab", true);
		for (String element: subSet) {
			System.out.println(element);
		}
	}

}
