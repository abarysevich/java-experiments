package test;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

public class TreeSetNotComparableEntryTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		NavigableSet<List<Object>> treeSet = new TreeSet<>();
		
//		Error on the runtime!!! Only objects which implement
//		comparable are possible!!!
		treeSet.add(new ArrayList<Object>());
//		treeSet.add(new ArrayList<Object>());
//		System.out.print(treeSet.first());
	}

}
