package test;

import java.util.LinkedHashMap;


public class LinkedHashMapTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LinkedHashMap<Integer, String> map = new LinkedHashMap<>();
		map.put(1, "first");
		map.put(3, "three");
		map.put(2, "two");

		// Nice shot!!!!
		for (Integer key: map.keySet()) {
			System.out.println(map.get(key));
		}
	}

}
