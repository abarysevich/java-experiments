package test;

import java.util.Deque;
import java.util.LinkedList;

public class DequeueTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Deque<Integer> dequeue = new LinkedList<>();
		
		// Retrieves but not removes
		
		// If empty - returnns null
		Integer value = dequeue.peek();
		value = dequeue.peekFirst();
		value = dequeue.peekLast();
		
//		Same functionality except: Throws exception java.util.NoSuchElementException if there is
//		no more element in queue - 
//		value = dequeue.getFirst();
//		value = dequeue.getLast();
//		System.out.println(value);
		
		value = dequeue.poll();
		value = dequeue.pollFirst();
		value = dequeue.pollLast();
		System.out.println(value);
		
//		Same functionality except: Throws exception java.util.NoSuchElementException if there is
//		no more element in queue - 
//		value = dequeue.removeFirst();
//		value = dequeue.removeLast();
//		value = dequeue.remove();
//		System.out.println(value);
		
		dequeue.add(1);
		dequeue.addLast(9);
		dequeue.addFirst(2);

		value = dequeue.poll();
		System.out.println(value);
		value = dequeue.pollFirst();
		System.out.println(value);
		value = dequeue.pollLast();
		System.out.println(value);

		System.out.println(dequeue);

		dequeue.offerLast(2);
		dequeue.offer(9);
		dequeue.offerFirst(1);

		value = dequeue.removeFirst();
		System.out.println(value);
		value = dequeue.removeLast();
		System.out.println(value);
		value = dequeue.remove();
		System.out.println(value);
		
		System.out.println(dequeue);
	}

}
