package test;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> strings = new LinkedList<>();
		strings.add("first");		
		strings.add("second");
		ListIterator<String> iter = strings.listIterator(strings.size());
		while (iter.hasPrevious()) {
			System.out.println(iter.previous());
		}
	}

}
