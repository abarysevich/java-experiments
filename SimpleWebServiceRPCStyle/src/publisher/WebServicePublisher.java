package publisher;

import javax.xml.ws.Endpoint;

import ch01.ts.TimeServerImpl;

public class WebServicePublisher 
{
	
	public static void main(String[] args) 
	{
		Endpoint.publish("http://127.0.0.1:9876/ts", new TimeServerImpl());
	}
	
}
