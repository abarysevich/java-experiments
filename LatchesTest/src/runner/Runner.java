package runner;

import java.util.concurrent.CountDownLatch;

public class Runner {
	private static final byte THREADS_COUNT = 10;
	private static final CountDownLatch lathce = new CountDownLatch(THREADS_COUNT);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int i=1;  i < THREADS_COUNT; i++) {
			final int threadNumber = i;
			Thread thread = new Thread(new Runnable(){
				@Override
				public void run() {
					try {
						lathce.await();
						System.out.println(String.format("Thread %d was executed", threadNumber));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			thread.start();
			lathce.countDown();
		}
		System.out.println("Resuming threads execution.");
		lathce.countDown();
	}

}
