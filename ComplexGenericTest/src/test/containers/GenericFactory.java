package test.containers;

public class GenericFactory<T> 
{
	
	public T getIstance(Class<T> clazz) throws InstantiationException, IllegalAccessException
	{
		// Creation instance by using T identifier is prohibited - reason is type erasure.
		// T instance = new T();
		return clazz.newInstance();
	}
	
}
