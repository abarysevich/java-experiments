package test.containers;

import test.boundaries.IUpperTestBoundable;

// Wildcard can't be used in class or methods declaration!!!
// public class WildcardContainer<? extends TestBoundableInterface>
public class WildcardContainer<T extends IUpperTestBoundable> 
{

	public <U> U print(U object)
	{
		return object;
	}
	
}

