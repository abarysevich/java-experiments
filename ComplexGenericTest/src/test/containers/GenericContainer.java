package test.containers;

public class GenericContainer<T> 
{
	
	private T containedObject;
	
	public T getObject()
	{
		return containedObject;
	}
	
	
	public void setObject(T object)
	{
		this.containedObject = object;
	}
	
}
