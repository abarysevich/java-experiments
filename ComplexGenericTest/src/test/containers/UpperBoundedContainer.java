package test.containers;

import test.boundaries.IUpperTestBoundable;

public class UpperBoundedContainer <T extends IUpperTestBoundable>  
{
	
	private T upperBoundedObject;
	
	public UpperBoundedContainer(T object)
	{
		upperBoundedObject = object;
	}

	public void print()
	{
		upperBoundedObject.upperBound();
	}
	
}
