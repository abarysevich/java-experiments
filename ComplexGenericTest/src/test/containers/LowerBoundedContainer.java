package test.containers;

import test.boundaries.ILowerTestBoundable;

public class LowerBoundedContainer <T extends ILowerTestBoundable>
{
	
	private T lowerBoundedObject;
	
	public LowerBoundedContainer(T object)
	{
		lowerBoundedObject = object;
	}
	
	public void print()
	{
		lowerBoundedObject.lowerBound();
	}
	
}
