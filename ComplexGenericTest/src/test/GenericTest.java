package test;

import test.boundaries.LowerBounded;
import test.boundaries.UpperBounded;
import test.containers.GenericContainer;
import test.containers.LowerBoundedContainer;
import test.containers.UpperBoundedContainer;

public class GenericTest 
{
	
	public static void testRaw()
	{
		GenericContainer cont = new GenericContainer();
		cont.setObject("Test");
		cont.getObject();
	}
	
	public static void testGeneric()
	{
		GenericContainer<String> cont = new GenericContainer<String>();
		cont.setObject("Test");
		cont.getObject();
	}
	
	public static void testWildcard()
	{
//		new GenericContainer<?>() in right sight can not been instantiated:
//		GenericContainer<?> cont = new GenericContainer<?>();
		GenericContainer<?> cont = new GenericContainer<String>();
		
		// Adding new element to parametrized method is prohibited because container 
		// after compilation must contain single element Type which can not be
		// determined in this case: cont.setObject("Test");
		
		// Object returned
		cont.getObject();
	}
	
	public static void testBoundedWildcard()
	{
//		new GenericContainer<? extends CharSequence>() in right sight can not been instantiated:
//		GenericContainer<?> cont = new GenericContainer<? extends CharSequence>();
		GenericContainer<? extends CharSequence> cont = new GenericContainer<String>();
		
		// Adding new element to parametrized method is prohibited because container
		// after compilation must contain 
		// single element Type which can not be determined in this case:
		// cont2.setObject("Test");
		
		// CharSequence returned 
		cont.getObject().length();
	}

	public static void testCommonBoundedGeneric()
	{
		// Erasure replaces  UpperBoundedContainer(T object) constructor by 
		// this one UpperBoundedContainer(IUpperTestBoundable object)
		// as a result only IUpperTestBoundable object can be used as 
		// constructor arg raw type can't be instantiated
		UpperBoundedContainer cont = new UpperBoundedContainer(new UpperBounded());
		
		//  Both UpperBounded and lower bounded can be used in instance creation.
		//  Class generic type can't differ from constructor argument type.  
//		UpperBoundedContainer<UpperBounded> cont2 = 
//			new UpperBoundedContainer<UpperBounded>(new LowerBounded());
//		
//		UpperBoundedContainer<LowerBounded> cont5 = 
//			new UpperBoundedContainer<LowerBounded>(new UpperBounded());
		
		UpperBoundedContainer<UpperBounded> cont3 = 
			new UpperBoundedContainer<UpperBounded>(new UpperBounded());
		
		UpperBoundedContainer<LowerBounded> cont4 = 
			new UpperBoundedContainer<LowerBounded>(new LowerBounded());
		
		cont.print();
		cont3.print();
		cont4.print();
	}
	
	public static void testLowerBoundedGeneric()
	{
		//  Both UpperBounded and lower bounded can be used in instance creation.
		//  Class generic type can't differ from constructor argument type.  
//		UpperBoundedContainer<UpperBounded> cont2 = 
//			new UpperBoundedContainer<UpperBounded>(new LowerBounded());
//		
//		UpperBoundedContainer<LowerBounded> cont5 = 
//			new UpperBoundedContainer<LowerBounded>(new UpperBounded());
		
		// Upper bounded is prohibited because is not allowed by generic boundary
//		LowerBoundedContainer<UpperBounded> cont3 = 
//			new LowerBoundedContainer<UpperBounded>(new UpperBounded());
		
		LowerBoundedContainer<LowerBounded> cont4 = 
			new LowerBoundedContainer<LowerBounded>(new LowerBounded());
		
		cont4.print();
	}
	
	public static void testUpperBoundedGeneric()
	{
		//  Both UpperBounded and lower bounded can be used in instance creation.
		//  Class generic type can't differ from constructor argument type.  
//		UpperBoundedContainer<UpperBounded> cont2 = 
//			new UpperBoundedContainer<UpperBounded>(new LowerBounded());
//		
//		UpperBoundedContainer<LowerBounded> cont5 = 
//			new UpperBoundedContainer<LowerBounded>(new UpperBounded());
		
		UpperBoundedContainer<LowerBounded> cont3 = 
			new UpperBoundedContainer<LowerBounded>(new LowerBounded());
		
		UpperBoundedContainer<LowerBounded> cont4 = 
			new UpperBoundedContainer<LowerBounded>(new LowerBounded());
		
		cont3.print();
		cont4.print();
	}
	
}
