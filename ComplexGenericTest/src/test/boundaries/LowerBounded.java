package test.boundaries;

public class LowerBounded implements ILowerTestBoundable
{

	@Override
	public void upperBound() 
	{
		System.out.println("upperBound()");
	}

	@Override
	public void lowerBound() 
	{
		System.out.println("lowerBound()");
	}

}
