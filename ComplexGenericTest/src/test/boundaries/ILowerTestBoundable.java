package test.boundaries;

public interface ILowerTestBoundable extends IUpperTestBoundable
{

	void lowerBound();
	
}
