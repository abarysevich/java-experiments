package sort;

import java.util.Arrays;

public class ShellSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] testArray = {3, 4, 1, 8, 2, 5, 7, 6, 10, 9};
		sort(testArray);
		System.out.println(Arrays.toString(testArray));
	}

	private static void sort(int[] array) {
		int arrayLength = array.length;
		int hInterval = 1;
		while (hInterval < arrayLength/3) {
			hInterval = hInterval * 3 + 1;
		}
		
		while(hInterval >= 1) {
			for(int i = 0; i<arrayLength; i++) {
				for (int j = i; j >= hInterval; j-=hInterval) {
					if (array[j] < array[j-hInterval]) {
						exchange(array, j, j-hInterval);
					}
				}
				
			}
			hInterval = hInterval/3;
		}
	}
	
	private static void exchange(int[] array, int firstIndex, int secondIndex) {
		int tempElement = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tempElement;
	}

}
