package sort;

import java.util.Arrays;

public class QuickSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO shuffle
		int[] array = {7,6,5,4,3,2,1};
		sort(array);
		System.out.print(Arrays.toString(array));
	}

	private static void sort(int[] array) {
		partition(array, 0, array.length-1);
	}
	
	private static void partition(int[] array, int baseElementIndex, int upperElementIndex) {
		if (upperElementIndex <= baseElementIndex) {
			return;
		}

		int baseElement = array[baseElementIndex];
		int upperPartitionBound = upperElementIndex;
		int lowerElementIndex = baseElementIndex + 1;
		while(true) {
			while(array[lowerElementIndex] < baseElement) {
				if (lowerElementIndex == upperPartitionBound) {
					break;
				}
				lowerElementIndex++;
			}
			while(array[upperElementIndex] > baseElement) {
				if (lowerElementIndex == baseElementIndex) {
					break;
				}
				upperElementIndex--;
			}

			if (lowerElementIndex >= upperElementIndex) {
				break;
			}
			exchange(lowerElementIndex, upperElementIndex, array);
		}

		exchange(baseElementIndex, upperElementIndex, array);
		// process left side partition
		partition(array, baseElementIndex, --upperElementIndex);

		// process right side partition
		partition(array, lowerElementIndex, upperPartitionBound);
	}
	
	private static void exchange(int lowerElement, int upperElement, int[]array) {
		int tempElement = array[lowerElement];
		array[lowerElement] = array[upperElement];
		array[upperElement] = tempElement;
	}
}
