package sort;

import java.util.Arrays;

public class MergeSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = {7,6,5,4,3,2,1};
		sort(array);
		System.out.print(Arrays.toString(array));
	}
	
	private static void sort(int[] array) {
		//TODO shuffle array!!!
		sort(0, array.length-1, array);
	}

	private static void sort(int lowElement, int highElelment, int[] array) {
		if(highElelment <= lowElement) {
			return;
		}
		
		int mid = (lowElement + highElelment)/2;
		// sort left side
		sort(lowElement, mid, array);
		 // sort right side
		sort(++mid, highElelment, array);
		merge(lowElement, highElelment, array);
	}

	private static void merge(int lowElement, int highElelment, int[] array) {
		int[] auxiliary = new int[highElelment - lowElement + 1];
		for (int i = lowElement, j=0; i <= highElelment; i++, j++) {
			auxiliary[j] = array[i];
		}
		
		int lowerAuxiliaryArrayIndex = 0;
		int auxiliaryMedium = (auxiliary.length%2==0)?auxiliary.length/2:auxiliary.length/2 + 1;
		int higherAuxiliaryArrayIndex = auxiliaryMedium;

		for(int currentIndex = lowElement; currentIndex <= highElelment; currentIndex++) {
			if(auxiliary[lowerAuxiliaryArrayIndex] <= auxiliary[higherAuxiliaryArrayIndex]) {
				array[currentIndex] = auxiliary[lowerAuxiliaryArrayIndex++];
				if(lowerAuxiliaryArrayIndex == auxiliary.length/2) {
					addArrayToTheTail(auxiliary, array, higherAuxiliaryArrayIndex, auxiliary.length,
							++currentIndex);
					break;
				}
			} else {
				array[currentIndex] = auxiliary[higherAuxiliaryArrayIndex++];
				if(higherAuxiliaryArrayIndex == auxiliary.length) {
					addArrayToTheTail(auxiliary, array, lowerAuxiliaryArrayIndex,
							auxiliaryMedium, ++currentIndex);
					break;
				}
			}
		}
	}
	
	private static void addArrayToTheTail(int[] from, int[] to, int startElementFrom, int finishElementExcluding,
			int startDestinationIndex){
		for (int i = startElementFrom; i < finishElementExcluding; i++) {
			to[startDestinationIndex++] = from[i];
		}
	}
	
}
