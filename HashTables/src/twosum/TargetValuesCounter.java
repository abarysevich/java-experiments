package twosum;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import reader.InputValuesReader;

public class TargetValuesCounter {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		List<Integer> result = InputValuesReader.readListOfNumbersFromFile("2Sum/HashInt.txt");
		Set<Integer> inputValuesDeduped = new HashSet<>(result);
		int searchingSumCount = 0;

		outer:
		for (int i=2500; i<=4000; i++) {
			inner:
			for (Integer currentValue: inputValuesDeduped) {
				if (i-currentValue == currentValue) {
					continue inner;
				}
				if (inputValuesDeduped.contains(i-currentValue)) {
					searchingSumCount++;
					continue outer;
				}
			}
		}
		System.out.println(searchingSumCount);
	}
}
