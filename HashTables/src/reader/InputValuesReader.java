package reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class InputValuesReader {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
//		String filePath = "2Sum/HashInt.txt";
		String filePath = "medianTracker/Median.txt";

		List<Integer> result = readListOfNumbersFromFile(filePath);
		System.out.println(result.size());
		System.out.println(result.get(0));
		System.out.println(result.get(result.size()-1));
	}

	public static List<Integer> readListOfNumbersFromFile(String filePath) 
			throws FileNotFoundException {
		List<Integer>  result = new ArrayList<>();
		Scanner scan = new Scanner(new File(filePath));
		while(scan.hasNextInt()) {
			result.add(scan.nextInt());
		}
		scan.close();
		return result;
	}
}
