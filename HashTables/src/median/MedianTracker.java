package median;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import reader.InputValuesReader;


public class MedianTracker {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		MedianTracker tracker = new MedianTracker();
		System.out.println(tracker.getMediansSum());
	}
	
	public int getMediansSum() throws FileNotFoundException {
		List<Integer> medians = getMedians(getNumberStream());
		return getMedianSumModThosand(medians);
	}
	
	private int getMedianSumModThosand(List<Integer> medians) {
		int medianSum = 0;
		for (Integer medianValue : medians) {
			medianSum+=medianValue;
		}
		return medianSum%10000;
	}
	
	private List<Integer> getMedians(Iterable<Integer> numbersStream) {
		PriorityQueue<Integer> lesserElementsPart = new PriorityQueue<>(5020);
		PriorityQueue<Integer> bigerElementsPart = new PriorityQueue<>(5020);
		List<Integer> medians = new ArrayList<>();
		
		Iterator<Integer> iterator = numbersStream.iterator();
		int firstElement = iterator.next();
		medians.add(firstElement);
		int secondElement = iterator.next();
		if (firstElement > secondElement) {
			medians.add(secondElement);
			lesserElementsPart.offer(-secondElement);
			bigerElementsPart.offer(firstElement);
		} else {
			medians.add(firstElement);
			lesserElementsPart.offer(-firstElement);
			bigerElementsPart.offer(secondElement);
		}
		
		while (iterator.hasNext()) {
			int nextElement = iterator.next();
			if (-lesserElementsPart.peek() > nextElement) {
				
				// if length lesser and bigger the same add to lesser
				if (lesserElementsPart.size() == bigerElementsPart.size()) {
					lesserElementsPart.offer(-nextElement);
				} else {
					// if length lesser > bigger (can be bigger only at one) add to lesser and 
					// move biggest element from lesser elements list to biigest list
					lesserElementsPart.offer(-nextElement);
					bigerElementsPart.offer(-lesserElementsPart.poll());
				}
				
			} else {
				// if length lesser and bigger the same add to lesser
				if (lesserElementsPart.size() == bigerElementsPart.size()) {
					if (nextElement > bigerElementsPart.peek()) {
						bigerElementsPart.offer(nextElement);
						lesserElementsPart.offer(-bigerElementsPart.poll());
					} else {
						lesserElementsPart.offer(-nextElement);
					}
				} else {
					// if length lesser > bigger (can be bigger only at one) add to bigger list
					bigerElementsPart.offer(nextElement);
				}
			}
			medians.add(-lesserElementsPart.peek());
		}
		assert medians.size() == 10000;
		return medians;
	}

	private static Iterable<Integer> getNumberStream() throws FileNotFoundException {
		return InputValuesReader.readListOfNumbersFromFile("medianTracker/Median.txt");
	}
}
