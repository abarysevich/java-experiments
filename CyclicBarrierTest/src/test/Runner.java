package test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Runner {
    private static final byte THREADS_COUNT = 2;

	private static final class Calculator {
		private long bound;
		private CyclicBarrier barrier;
		
		public Calculator(long bound, CyclicBarrier barrier) {
			this.bound = bound;
			this.barrier = barrier;
		}
		
	    void calculate() {
			for(int i=1; i < bound; i++) {
				double result = bound / i;
			}
			try {
				int reachedBarrier = barrier.await();
				System.out.println(String.format("Thread with id %d has reached barrier %d",
					Thread.currentThread().getId(), reachedBarrier));
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	} 

	public static void main(String[] args) {
		final CyclicBarrier barrier = new CyclicBarrier(THREADS_COUNT, new Runnable() {
			@Override
			public void run() {
				System.out.println("Barrier was passed!");
			}
		});
		
		for (int i = 0; i < THREADS_COUNT; ++i) {
			Thread thread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					Calculator calc = new Calculator(100000000, barrier);
					calc.calculate();
				}
			});
			thread.start();
		}
	}

}
