package jpa.dao;

import jpa.model.obj.SecondEntity;

public class SecondEntityDAO extends GeneralDAO<SecondEntity> 
{

	public SecondEntityDAO() 
	{
		super(SecondEntity.class);
	}
	
	public void persistSecondEntity(SecondEntity secEnt)
	{
		save(secEnt);
	}
	

}
