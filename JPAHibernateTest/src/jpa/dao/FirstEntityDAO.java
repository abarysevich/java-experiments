package jpa.dao;

import java.util.List;

import jpa.model.obj.FirstEntity;

public class FirstEntityDAO extends GeneralDAO<FirstEntity> 
{
	
	private static final String  FIND_RIRST_ENTITY = "findFirstEntityByPK";
	
	public FirstEntityDAO()
	{
		super(FirstEntity.class);
	}
	
	
	public List<FirstEntity> getFirstEntities()
	{
		return findByNamedQuery(FIND_RIRST_ENTITY);
	}
	

	public void deleteEntityById(byte id)
	{
		deleteById(id);
	}

	
}
