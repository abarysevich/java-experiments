package jpa.service;

import java.util.List;

import jpa.dao.FirstEntityDAO;
import jpa.dao.SecondEntityDAO;
import jpa.model.obj.FirstEntity;
import jpa.model.obj.SecondEntity;

public class OracleTempService
{
	
	private FirstEntityDAO firstEntityDAO;
	private SecondEntityDAO secondEntityDAO;
	
	public void setSecondEntityDAO(SecondEntityDAO secondEntityDAO) 
	{
		this.secondEntityDAO = secondEntityDAO;
	}
	
	public SecondEntityDAO getSecondEntityDAO() 
	{
		return secondEntityDAO;
	}

	public void setFirstEntityDAO(FirstEntityDAO firstEntityDAO) 
	{
		this.firstEntityDAO = firstEntityDAO;
	}

	public FirstEntityDAO getFirstEntityDAO() 
	{
		return firstEntityDAO;
	}
	
	
	public List<FirstEntity> getFirstEntities()
	{
		return firstEntityDAO.getFirstEntities();
	}
	
	public void deleteFirstEntityById(byte id)
	{
		firstEntityDAO.deleteById(id);
	}
	
	public void persistSecondEntity(SecondEntity secEnt)
	{
		secondEntityDAO.persistSecondEntity(secEnt);
	}
	
}
