package jpa.model.obj;

import java.io.Serializable;

public class SecondEntity implements Serializable
{
	
	private static final long serialVersionUID = 8388373751050897265L;
	private short pk;
	private String property;

	public void setPk(short pk) 
	{
		this.pk = pk;
	}

	public short getPk() 
	{
		return pk;
	}

	public void setProperty(String property)
	{
		this.property = property;
	}

	public String getProperty() 
	{
		return property;
	}
	
}
