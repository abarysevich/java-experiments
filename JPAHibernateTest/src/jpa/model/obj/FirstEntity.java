package jpa.model.obj;

import java.io.Serializable;
import java.util.Set;

public class FirstEntity implements Serializable
{
	
	private static final long serialVersionUID = -4891540340435308031L;
	private short pk;
	private String property;
	private Set<SecondEntity> seconEntitites;
	
	public void setPk(short pk) 
	{
		this.pk = pk;
	}
	
	public short getPk() 
	{
		return pk;
	}

	public void setProperty(String property)
	{
		this.property = property;
	}

	public String getProperty() 
	{
		return property;
	}

	public void setSeconEntitites(Set<SecondEntity> seconEntitites)
	{
		this.seconEntitites = seconEntitites;
	}

	public Set<SecondEntity> getSeconEntitites() 
	{
		return seconEntitites;
	}

}
