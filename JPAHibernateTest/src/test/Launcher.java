package test;

import jpa.model.obj.FirstEntity;
import jpa.model.obj.SecondEntity;
import jpa.service.OracleTempService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Launcher 
{
	
	private static final String APPLICATION_CONTEXT = "jpa_hibernate.xml";
	private static ApplicationContext context;
	
	static 
	{
		context = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		OracleTempService service = context.getBean(OracleTempService.class);
		for(FirstEntity fe: service.getFirstEntities())
		{
			System.out.println(fe.getProperty());
		}
		
		service.deleteFirstEntityById((byte) 1);
		
		SecondEntity secondEntity = new SecondEntity();
		secondEntity.setProperty("TestProperty");
		secondEntity.setPk((short)22);
		service.persistSecondEntity(secondEntity);
	}

}
