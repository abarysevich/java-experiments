package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {		
		int port = 10002;
		
		try (ServerSocket server = new ServerSocket(port)){
			Socket clientSocket = server.accept();
			Scanner scan = new Scanner(clientSocket.getInputStream());
			while (scan.hasNextLine()) {
				System.out.println(scan.nextLine());
			}
			scan.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
