package com.borex;


import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Pojo {

    public static Pojo create(String name, int numberOfLegs) {
        return new AutoValue_Pojo(name, numberOfLegs);
    }

    public abstract String name();
    public abstract int numberOfLegs();
}
