package com.borex.test;

import com.borex.Pojo;

/**
 * Created by abarysevich on 4/21/2018.
 */
public class Launcher {
    public static void main(String[] args) {
        Pojo pojo = Pojo.create("name", 4);
        System.out.println(pojo.toString());
    }
}
