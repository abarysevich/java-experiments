package util;

import java.net.URISyntaxException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.borsoft.IJdbcSchemaDAO;



public class Launcher 
{
	
//	private static final String ANNOTATION_BASED_SPRING_CONFIG = "src\\ioc-context-annotation-based.xml";
	private static final String DECLARATIVE_SPRING_CONFIG = "src\\ioc-context-declarative.xml";
	
	private static IJdbcSchemaDAO dao;
	
	
	static
	{
		ApplicationContext context = new FileSystemXmlApplicationContext(DECLARATIVE_SPRING_CONFIG);
//		ApplicationContext context = new FileSystemXmlApplicationContext(ANNOTATION_BASED_SPRING_CONFIG);
		dao = (IJdbcSchemaDAO)context.getBean("com.borsoft.JdbcSchemaDAO");
		
	}

	/**
	 * @param args
	 * @throws URISyntaxException 
	 */
	public static void main(String[] args) throws URISyntaxException 
	{
		System.out.println(dao.getAthourEntiresCount());
	}

}
