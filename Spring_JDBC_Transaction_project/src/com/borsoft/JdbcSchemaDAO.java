package com.borsoft;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

public class JdbcSchemaDAO implements IJdbcSchemaDAO
{

	private static final String FIRST_ENTITY_COUNT_QUERY = "select count(*) from FIRST_ENTITY"; 
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() 
	{
		return jdbcTemplate;
	}
	
	/* (non-Javadoc)
	 * @see com.borsoft.IJdbcSchemaDAO#getFirstEntityCount()
	 */
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public int getFirstEntityCount()
	{
		return jdbcTemplate.queryForInt(FIRST_ENTITY_COUNT_QUERY);
	}
	
}
