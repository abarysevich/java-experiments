package test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

public class Test {

	/**
	 * @param args
	 * @throws UnknownHostException 
	 * @throws TextParseException 
	 */
	public static void main(String[] args) throws UnknownHostException, TextParseException {
//		String domain = "trushel.net";
		String domain = "volat.io";
		
		// JSE DNS lookup test:
		InetAddress[] addresses = InetAddress.getAllByName(domain);
		for (InetAddress address : addresses) {
			System.out.println(address.toString());
			System.out.println(address.getHostName());
		}
		
		// MX records fetching
		getMXRecord(domain);
	}
	
	private static void getMXRecord(String name) throws TextParseException, UnknownHostException {
		Lookup lookup = new Lookup(name, Type.MX);
		Resolver resolver = new SimpleResolver();
//		lookup.setResolver(resolver);
//		lookup.setCache(null);
		for (Record dnsRecord: lookup.run()) {
			MXRecord record = (MXRecord)dnsRecord;
			System.out.println(record.getPriority() + " " + record.rdataToString());
		}
	}
}
