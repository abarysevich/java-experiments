package test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import reader.DijkstraGraphReader;

import dijkstra.DijkstraShortestPathsAlgorithm;
import dijkstra.DijkstraShortestPathsAlgorithm.Vertice;

public class DijkstraRunner {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws FileNotFoundException, CloneNotSupportedException {

		Map<Integer, Vertice> graph = 
				DijkstraGraphReader.readGraphFromFile("dijkstra/dijkstraData.txt");
		DijkstraShortestPathsAlgorithm dijkstra = new DijkstraShortestPathsAlgorithm(graph);
		List<Vertice> graphshortestPaths = dijkstra.getVerticiesWithShortetPath(graph.get(1));
		Integer[] requiredVerticiesIndexes = {7,37,59,82,99,115,133,165,188,197};
		List<Integer> requiredVerticiesIndexesList = Arrays.asList(requiredVerticiesIndexes);
		for(Vertice vertice: graphshortestPaths) {
			if (requiredVerticiesIndexesList.contains(vertice.getName()))
				System.out.println(vertice);
//			7,37,59,82,99,115,133,165,188,197
//			7,    37,   59,   82,   99,   115,  133,  165,  188,  197
//			2599, 2610, 2947, 2052, 2367, 2399, 2029, 2442, 2610, 3068	   - 1-st attempt
//															2505           - 2-nd attempt
		}
		System.out.println(graphshortestPaths.size());
	}

}
