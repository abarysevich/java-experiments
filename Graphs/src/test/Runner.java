package test;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import counter.GraphMinimumCutCounter;

import reader.GraphReader;

public class Runner {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
//		17 is minimum cut for this
		String filePath = "minimumCuts/kargerMinCut.txt";
//		String filePath = "minimumCuts/testMinCut.txt";
		int timesToRun = 1000000;
		Map<Integer, Integer> results = new TreeMap<>();
		
		GraphMinimumCutCounter minimumCutCounter = new GraphMinimumCutCounter();
		
		while(timesToRun > 0) {
			int minimumCut = minimumCutCounter.countMinimumCut(
					GraphReader.readGraphsAdjacentList(filePath));
			Integer previousSameMinCutsCountAttemts = results.get(minimumCut);
			if (previousSameMinCutsCountAttemts == null) {
				results.put(minimumCut, Integer.valueOf(1));
			} else {
				results.put(minimumCut, ++previousSameMinCutsCountAttemts);
			}
			timesToRun--;
		}
		
		for(Entry<Integer, Integer> entry : results.entrySet()){
			System.out.println(String.format("Resuld %d achieved from next number of computations %d",
					entry.getKey(), entry.getValue()));
		}
	}

}
