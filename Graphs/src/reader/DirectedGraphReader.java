package reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class DirectedGraphReader {
	public static DirectedGraph<Integer, DefaultEdge> readGraph(String filePath) 
			throws FileNotFoundException {
		DirectedGraph<Integer, DefaultEdge> graph = 
				new DefaultDirectedGraph<>(DefaultEdge.class);
		
	    List<Edge<Integer, Integer>> edges = readEdges(filePath);
		for(Edge<Integer, Integer> edge: edges) {
			graph.addVertex(edge.begin);
			graph.addVertex(edge.end);
			graph.addEdge(edge.begin, edge.end);
		}
		return graph;
	}
	
	public static List<Edge<Integer, Integer>> readEdges(String filePath) 
			throws FileNotFoundException {
		List<Edge<Integer, Integer>> result = new ArrayList<>();
		Scanner scan = new Scanner(new File(filePath));
		while(scan.hasNextLine()) {
			processLine(scan.nextLine(), result);
		}
		scan.close();
		return result;
	}

	private static void processLine(String line, List<Edge<Integer, Integer>> result){
		String[] verticies = line.split(" ");
		Edge<Integer, Integer> edge = new Edge<>();
		edge.setBegin(Integer.valueOf(verticies[0]));
		edge.setEnd(Integer.valueOf(verticies[1]));
		result.add(edge);
	} 
	
	private static class Edge<B, E> {
		private B begin;
		private E end;
		
		public void setBegin(B begin) {
			this.begin = begin;
		}

		public void setEnd(E end) {
			this.end = end;
		}
	}
	
}
