package reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class GraphReader {

	public static Map<Integer, List<Integer>> readGraphsAdjacentList(String filePath)
			throws FileNotFoundException {
		Map<Integer, List<Integer>> result = new HashMap<>(); 
		Scanner scan = new Scanner(new File(filePath));
		while(scan.hasNextLine()) {
			processAdjacentGraphLine(scan.nextLine(), result);
		}
		scan.close();
		return result;
	}
	
	private static void processAdjacentGraphLine(String line, 
			Map<Integer, List<Integer>> result) {
		String[] graphWithAdjacents = line.split("	");
		List<Integer> lineContent = convertStringArrayToIntList(
				Arrays.copyOfRange(graphWithAdjacents, 1, graphWithAdjacents.length));
		result.put(Integer.valueOf(graphWithAdjacents[0]), lineContent);
	}
	
	private static List<Integer> convertStringArrayToIntList(String[] graphWithAdjacents) {
		List<Integer> result = new ArrayList<>();
		for(String element: graphWithAdjacents) {
			if("".equals(element)) {
				continue;
			}
			result.add(Integer.valueOf(element));
		}
		return result;
	}

	public static void main(String[] args) throws FileNotFoundException {
		Map<Integer, List<Integer>>  graphs = readGraphsAdjacentList("minimuCuts/kargerMinCut.txt");
		System.out.println(String.format("Graphs count is %d", graphs.keySet().size()));
		for(Entry<Integer, List<Integer>> entry : graphs.entrySet()) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
		}
	}
	
}
