package dijkstra;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class DijkstraShortestPathsAlgorithm {
	private PriorityQueue<Vertice> notYetProcessedVerticies = new PriorityQueue<>(200);
	private List<Vertice> processedVerticies = new ArrayList<>();
	private Map<Integer, Vertice> graph;
	
	public DijkstraShortestPathsAlgorithm(Map<Integer, Vertice> graph) {
		this.graph = graph;
	}

	public List<Vertice> getVerticiesWithShortetPath(Vertice baseVertice) throws CloneNotSupportedException {
		findShortestPathsFromVertice(baseVertice);
		return processedVerticies;
	}

	private void findShortestPathsFromVertice(Vertice baseVertice) throws CloneNotSupportedException {
		baseVertice.distance = 0;
		processVertice(baseVertice);
		while (!notYetProcessedVerticies.isEmpty()) {
			processVertice(notYetProcessedVerticies.poll());
		}
	}

	private void processVertice(Vertice vertice) throws CloneNotSupportedException {
		// put all adjacent verticies in the not yet processed queue - if queue contains 
		Map<Integer, Integer> adjacentVerticiesWithDistancies = 
				vertice.getAdjacentVerticies();
		for (Integer adjacentVerticeName: adjacentVerticiesWithDistancies.keySet()) {
			// Fetch adjacent vertice from graph
			Vertice adjacentVertice = graph.get(adjacentVerticeName);

			// check wasn't it processed before?!!
			if (wasVerticeProcessedBefore(adjacentVertice)) {
				continue;
			}
			
			// Update adjacent vertice distances (add distance between adjacent and current vertice distance)
			int pathFromRootToAdjacent = vertice.distance + getDistanceBetweenVerticies(vertice, 
					adjacentVertice);
			// current adjacent vertice can be lesser than summ !! (check this)
			// if not initialized yet update with vertice distance + distance between vertice and adjacent
			
			// TODO pass this distance not via adjacentVertice but as input parameter
			if(adjacentVertice.distance==-1) {
				adjacentVertice.distance = pathFromRootToAdjacent;
			} else {
//				else choose shortest one
				if (adjacentVertice.distance > pathFromRootToAdjacent) {
					adjacentVertice.distance = pathFromRootToAdjacent;
				}
			}
			addOrUpdateExistingVerticeToNotYetProcessed(adjacentVertice);
		}
		processedVerticies.add(vertice);
	}
	
	private int getDistanceBetweenVerticies(Vertice startVertice, Vertice endVertice) {
		return startVertice.adjacentVerticiesDistances.get(endVertice.name);
	}
	
	private boolean wasVerticeProcessedBefore(Vertice adjacentVertice) {
		return processedVerticies.contains(adjacentVertice);
	}

	private void addOrUpdateExistingVerticeToNotYetProcessed(Vertice adjacentVertice)
			throws CloneNotSupportedException {
		// vertice with the same name update it's value if it is already known!!
		if (notYetProcessedVerticies.contains(adjacentVertice)) {
			Vertice queueVertice = 
					getObjectFromQueue(notYetProcessedVerticies, adjacentVertice);
			if (queueVertice.distance > adjacentVertice.distance) {
				// Heapify heap (PriorityQueue) after element updating!!!
				notYetProcessedVerticies.remove(queueVertice);
				queueVertice.distance = adjacentVertice.distance;
				notYetProcessedVerticies.offer(queueVertice);
			}
		} else {
			notYetProcessedVerticies.offer((Vertice) adjacentVertice.clone());
		}
	}
	
	private <T> T getObjectFromQueue(Queue<T> queue, T object) {
		for(T queueObject: queue) {
			if(queueObject.equals(object)) {
				return queueObject;
			}
		}
		throw new IllegalArgumentException();
	}
	
	
	public static class Vertice implements Comparable<Vertice>, Cloneable {
		private Integer name;
		private Integer distance = Integer.valueOf(-1);
		private Map<Integer, Integer> adjacentVerticiesDistances;
		
		public Vertice(Integer name,
				Map<Integer, Integer> adjacentVerticiesDistances) {
			this.name = name;
			this.adjacentVerticiesDistances = adjacentVerticiesDistances;
		}
		
		public Vertice(Integer name) {
			this.name = name;
		}

		@Override
		public int compareTo(Vertice o) {
			return distance.compareTo(o.distance);
		}
		
		@Override
		public String toString() {
			return String.format("Vertice with name %d current distance is %d", this.name,
					this.distance);
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Vertice) {
				return ((Vertice) obj).name.equals(this.name);
			}
			throw new IllegalArgumentException();
		}

		@Override
		public Object clone() throws CloneNotSupportedException {
			return super.clone();
		}

		public Map<Integer, Integer> getAdjacentVerticies() {
			return adjacentVerticiesDistances;
		}
		
		public Integer getName() {
			return this.name;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PriorityQueue<Vertice> notYetProcessedVerticies = new PriorityQueue<>(200);
		
		notYetProcessedVerticies.add(new Vertice(1));
		notYetProcessedVerticies.add(new Vertice(2));
		notYetProcessedVerticies.add(new Vertice(3));
		
		
		while (!notYetProcessedVerticies.isEmpty()) {
			System.out.println(notYetProcessedVerticies.poll());
		}
	}
	
}
