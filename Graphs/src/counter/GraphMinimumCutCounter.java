package counter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public class GraphMinimumCutCounter {
	private Random random = new Random();

	public int countMinimumCut(Map<Integer, List<Integer>> graph) {
		if(graph.keySet().size() < 2) {
			throw new RuntimeException();
		}
		
		// TODO(abarysevich) check loop work 
		while(graph.keySet().size()>2) {
			randomlyFuseTwoVertices(graph);
		}
		
		Set<Integer> keysSet = graph.keySet();
		assert keysSet.size() == 2;
		return graph.get(keysSet.iterator().next()).size();
	}
	
	private void randomlyFuseTwoVertices(Map<Integer, List<Integer>> graph) {
		// choose vertice randomly
		int firstVertice = randomlyChooseVertice(graph);
		// choose in choosen vertice edje randomly (other vertice from list)
		int secondVertice = randomlyChooseVerticeFromList(graph.get(firstVertice));
		fuseVertices(firstVertice, secondVertice, graph);
	}
	
	// fuse vertices which are connected with this randomly choosen edje
	// merge adjacent lists from both to the result one - remove loop vertices
	private void fuseVertices(int firstVertice, int secondVertice,
			Map<Integer, List<Integer>> graph) {
		
		List<Integer> firstAdjasentsList = graph.get(firstVertice);
		List<Integer> secondAdjasentsList = graph.get(secondVertice);
		firstAdjasentsList.addAll(secondAdjasentsList);
		
		// replace in all other adjacent lists this element index
		replaceInAllListsVerticeIndexWithNewOne(secondVertice, firstVertice, graph);

//		remove cyclick references from all adjactements list
		removeCyclicEdgeFromList(graph);
		
		graph.remove(secondVertice);
		graph.put(firstVertice, firstAdjasentsList);
	}
	
	private void replaceInAllListsVerticeIndexWithNewOne(int verticeToReplace,
			int verticeToReplaceWith, Map<Integer, List<Integer>> graph) {
		for(List<Integer> adjacentsList: graph.values()) {
			if(adjacentsList.contains(verticeToReplace)) {
				for(int i=0; i<adjacentsList.size(); i++) {
					if(adjacentsList.get(i).equals(verticeToReplace)) {
						adjacentsList.set(i, verticeToReplaceWith);
					}
				}
			}
		}
	}
	
	private void removeCyclicEdgeFromList(Map<Integer, List<Integer>> graph) {
		for(Entry<Integer, List<Integer>> entry:  graph.entrySet()) {
			entry.getValue().removeAll(Collections.singleton(entry.getKey()));
		}
	}
	
	private int randomlyChooseVertice(Map<Integer, List<Integer>> graph) {
		Object vertice = graph.keySet().toArray()[random.nextInt(graph.keySet().size())];
		return ((Integer)vertice).intValue();
	}
	
	private int randomlyChooseVerticeFromList(List<Integer> adjacentsList) { 
		return adjacentsList.get(random.nextInt(adjacentsList.size()));
	}
}
