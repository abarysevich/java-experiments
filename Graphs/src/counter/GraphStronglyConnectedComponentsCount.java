package counter;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;

import reader.DirectedGraphReader;

public class GraphStronglyConnectedComponentsCount {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		DirectedGraph<Integer, DefaultEdge> graph = 
				DirectedGraphReader.readGraph("stronglyConnectedComponents/SCC.txt");
		StrongConnectivityInspector<Integer, DefaultEdge> inspector =
				new StrongConnectivityInspector<>(graph);
		
		List<Set<Integer>> stronglyConnectedSets = inspector.stronglyConnectedSets();

		for(Integer size: getSortedSizes(stronglyConnectedSets)) {
			System.out.println(size);
		}
	}

	private static Set<Integer> getSortedSizes(List<Set<Integer>> stronglyConnectedSets) {
		Set<Integer> sortedSet = new TreeSet<>();
		for(Set<Integer> stronglyConnectedSet: stronglyConnectedSets) {
			sortedSet.add(stronglyConnectedSet.size());
		}
		return sortedSet;
	}
}
