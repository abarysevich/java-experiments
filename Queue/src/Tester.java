import java.util.Iterator;


public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//
//		Deque<String> deque = new Deque<>();
//		deque.addFirst("o");
//		deque.addLast("o");
//		
//		deque.removeFirst();
//		
//		deque.addFirst("o");
//		deque.addLast("o");
//		
//		deque.removeLast();
//		
//		deque.addFirst("o");
//		deque.addLast("o");
//
//		Iterator<String> iterator = deque.iterator();
//		while (iterator.hasNext()) {
//			System.out.println(iterator.next());
//		}
//
//		deque.removeLast();
//		deque.removeLast();
//		deque.removeLast();
//
//		System.out.println(deque.isEmpty());
//
//		deque.addFirst("o");
//
//		System.out.println(deque.isEmpty());
//
//		deque.removeLast();
//		
//		System.out.println(deque.isEmpty());
		
		
		RandomizedQueue<String> queue = new RandomizedQueue<>();
		queue.enqueue("w");
		queue.enqueue("o");
		queue.enqueue("f");
		
		queue.dequeue();
		queue.enqueue("r");
		queue.dequeue();
		queue.enqueue("t");
		queue.dequeue();
		queue.enqueue("m");
		queue.enqueue("n");

		
		queue.iterator();
		Iterator<String> iterator = queue.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		Iterator<String> iterator2 = queue.iterator();
		while (iterator2.hasNext()) {
			System.out.println(iterator2.next());
		}
	}

}
