
public class Subset {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int elementsToPrint = Integer.valueOf(args[0]);
		
		RandomizedQueue<String> queue = new RandomizedQueue<String>();

		while (!StdIn.isEmpty()) {
			queue.enqueue(StdIn.readString());
		}
		
		for(int i=0; i < elementsToPrint; i++) {
			System.out.println(queue.dequeue());
		}
	}

}
