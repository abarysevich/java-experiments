import java.util.Iterator;
import java.util.NoSuchElementException;


public class Deque<Item> implements Iterable<Item> {
	private Entry<Item> first;
	private Entry<Item> last;
	private int size;
	
	// is the deque empty?
	public boolean isEmpty() {
		return size == 0;
	}
	
	// return the number of items on the deque
	public int size() {
		return this.size;
	}
	
	// insert the item at the front
	public void addFirst(Item item) {
		if (item == null) {
			throw new NullPointerException();
		}
		Entry<Item> newEntry = new Entry<Item>(item);
		if (isEmpty()) {
			this.first = newEntry;
			this.last = newEntry;
		} else {
			Entry<Item> tempEntry = this.first;
			this.first = newEntry;
			newEntry.nextEntry = tempEntry;
			tempEntry.previousEntry = newEntry;
		}
		this.size++;
	}
	
	// insert the item at the end
	public void addLast(Item item) {
		if (item == null) {
			throw new NullPointerException();
		}
		Entry<Item> newEntry = new Entry<Item>(item);
		if (isEmpty()) {
			this.first = newEntry;
			this.last = newEntry;
		} else {
			Entry<Item> tempEntry = this.last;
			this.last = newEntry;
			newEntry.previousEntry = tempEntry; 
			tempEntry.nextEntry = newEntry;
		}
		this.size++;
	}
	
	// delete and return the item at the front
	public Item removeFirst() {
		if(isEmpty()) {
			throw new NoSuchElementException();
		} else {
			Entry<Item> firstEntry = this.first;
			this.first = firstEntry.nextEntry;
			if (this.first == null) {
				this.last = null;
			}
			this.size--;
			return firstEntry.item;
		}
	}
	
	// delete and return the item at the end
	public Item removeLast() {
		if(isEmpty()) {
			throw new NoSuchElementException();
		} else {
			Entry<Item> lastEntry = this.last;
			this.last = lastEntry.previousEntry;
			if (this.last == null) {
				this.first = null;
			}
			this.size--;
			return lastEntry.item;
		}
	}

	private class Entry<L> {
		Entry<L> previousEntry;
		Entry<L> nextEntry;
		L item; 

		public Entry(L item) {
			this.item = item;
		}
	}
	
	@Override
	public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			Entry<Item> currentEntry = first;
			
			@Override
			public boolean hasNext() {
				return !isEmpty() && currentEntry != null;
			}

			@Override
			public Item next() {
				if (isEmpty() || currentEntry == null) {
					throw new NoSuchElementException();
				}
				Item item = currentEntry.item;
				currentEntry = currentEntry.nextEntry;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}
