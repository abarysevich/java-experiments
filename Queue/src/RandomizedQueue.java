import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;


public class RandomizedQueue<Item> implements Iterable<Item> {
	   private Random rand = new Random();
	   private Item[] entries;
	   private int elementsCount;

       // construct an empty randomized queue
	   @SuppressWarnings("unchecked")
	   public RandomizedQueue() {
		   this.elementsCount = 0;
		   this.entries = (Item[])new Object[10];
	   }
	   
	   // is the queue empty?
	   public boolean isEmpty() {
		   return this.elementsCount == 0;
	   }
	   
	   // return the number of items on the queue
	   public int size() {
		   return this.elementsCount;
	   }
	   
	   // add the item
	   public void enqueue(Item item) {
		   if (item == null) {
			   throw new NullPointerException();
		   }
		   if(this.elementsCount == this.entries.length) {
			   doubleArrayLength();
		   }
		   this.entries[this.elementsCount] = item;
		   this.elementsCount++;
	   }
	   
	   private void doubleArrayLength() {
		   this.entries = Arrays.copyOf(entries, this.entries.length*2);
	   }

	   // delete and return a random item
	   public Item dequeue() {
		   if (this.elementsCount == 0) {
				throw new NoSuchElementException();
			}
		   int elementIndex = rand.nextInt(this.elementsCount);
		   Item randomEntry = this.entries[elementIndex];
		   // Just replace current random entry with the last queue entry since the
		   // order is not significant for randomized queue
		   // Elements shift operation has linear (O(N)) complexity :)
		   if (--this.elementsCount != 0) {
			   this.entries[elementIndex] = this.entries[this.elementsCount];
			   this.entries[this.elementsCount] = null;
			   adjustArraySize();
		   }
		   return randomEntry;
	   }
	   
	   private void adjustArraySize() {
		   if (this.elementsCount <= this.entries.length / 4) {
			   this.entries = Arrays.copyOf(this.entries, this.entries.length / 2);
		   }
	   }

	   // return (but do not delete) a random item
	   public Item sample() {
		   if (this.elementsCount == 0) {
				throw new NoSuchElementException();
			}
		   return this.entries[rand.nextInt(this.elementsCount)];
	   }
	   
	   // return an independent iterator over items in random order
	   public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			
			int startIndex = rand.nextInt(elementsCount);
			int currentIndex = 0;
			
			@Override
			public boolean hasNext() {
				return currentIndex < elementsCount;
			}

			@Override
			public Item next() {
				if (currentIndex >= elementsCount) {
					throw new NoSuchElementException();
				}
				int elementIndex = (currentIndex++ + startIndex) % elementsCount;
				return entries[elementIndex];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		   
	   }
	   
}
