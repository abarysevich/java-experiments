package test;

import java.util.concurrent.*;

public class Test {
    private static ExecutorService executor =
            Executors.newSingleThreadExecutor();

    public static void main(String[] args) throws Exception {
        // Examples and docs are taken from here:
        // https://www.callicoder.com/java-8-completablefuture-tutorial/
        CompletableFuture<String> completableFuture =
                new CompletableFuture<>();

        executor.execute(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            completableFuture.complete("Out of exuted!!!");
        });

        System.out.println(completableFuture.get());


        CompletableFuture.runAsync(() -> {
                    System.out.println("Inside runAsync");
                }
        );

        CompletableFuture<String> result = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Result";
        }).thenApplyAsync(futureResult -> {
            return String.format("Applied async to result: %s", futureResult);
        }).thenApply(thenApplyAsyncResult -> {
            return String.format("Applied to result: %s", thenApplyAsyncResult);
        });
//        .thenAccept(finalResult -> {
//                    System.out.println(finalResult);
//        });

        System.out.println(String.format("From async supplier: %s", result.get()));

        CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                return "Result to thenAccept";
            }
        ).thenAccept(System.out::println);


        CompletableFuture.allOf(
                CompletableFuture.runAsync(()->
                    {
                        System.out.println("From first completable future!");
                    }),
                CompletableFuture.runAsync(()->
                    {
                        System.out.println("From second completable future!");
                    })
                ).join();

        executor.shutdown();
    }
}
