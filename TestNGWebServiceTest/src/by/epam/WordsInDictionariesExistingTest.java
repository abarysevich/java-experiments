package by.epam;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;

import com.aonaware.services.webservices.ArrayOfDictionary;
import com.aonaware.services.webservices.ArrayOfDictionaryWord;
import com.aonaware.services.webservices.ArrayOfStrategy;
import com.aonaware.services.webservices.DictService;
import com.aonaware.services.webservices.DictServiceSoap;
import com.aonaware.services.webservices.Dictionary;
import com.aonaware.services.webservices.DictionaryWord;
import com.aonaware.services.webservices.Strategy;


public class WordsInDictionariesExistingTest 
{
	// xml file path with input words.
	private static final String XMLFILEPATH = "WordsList.xml";
	
	// web service object.
	private DictServiceSoap dictServiceSoap;
	
	// Strategies list.
	private List<Strategy> strategiesList;
	
	// Dictionaries list.
	private List<Dictionary> dictionariesList;
	
	/**
	 * Creates input parameters for testing. List of dictionaries, list of words 
	 * and list of strategies.
	 * 
	 * @return Object[][] - dictionaries with words to search in using specified
	 *  strategy.
	 */
	@DataProvider(name = "InputData")
	public Object[][] createData()
	{
		List<String> words = XMLFileUtil.getInputWords(XMLFILEPATH);
		
		int length = this.dictionariesList.size()*this.strategiesList.size()*words.size(); 
		Object[][] inputParams = new Object [length][2]; 
		int counter = 0;
		
		for(String word: words)
		{
			for(Dictionary dict: this.dictionariesList)
			{
				for(Strategy strat: this.strategiesList)
				{
					inputParams[counter][0] = new Object[][]{{dict, strat}};
					inputParams[counter][1] = word;
					counter++;
				}
			}
		}
		
		return inputParams;
	}
	
	/**
	 * Initialize all input parameters and Web service object. List of dictionaries and List 
	 * of strategies are requested from web service. List of words is retrieved from xml file.
	 */
	@BeforeClass
	public void before()
	{
		DictService service = new DictService();
		this.dictServiceSoap = service.getDictServiceSoap();
		
		ArrayOfStrategy strategiesScope = dictServiceSoap.strategyList();
		ArrayOfDictionary dictionariesScope = dictServiceSoap.dictionaryList();
		
		this.strategiesList = strategiesScope.getStrategy();
		this.dictionariesList = dictionariesScope.getDictionary();
	}
	/**
	 * Search word in dictionary using specified strategy.
	 * @param dictionaryAndStrategy dictionary to search in and strategy for search.
	 * @param word word to search.
	 */
	@Test(dataProvider="InputData")
	public void test(Object[][] dictionaryAndStrategy, String word)
	{
		Dictionary dictionary = (Dictionary)dictionaryAndStrategy[0][0];
		Strategy strategy = (Strategy)dictionaryAndStrategy[0][1];
		
		ArrayOfDictionaryWord array = dictServiceSoap.matchInDict(dictionary.getId(), word, strategy.getId());
		Assert.assertTrue(checkWordExistenceInTheDictionaryUsingSpecStrategy(dictionary, strategy, word, array),
						  "The word " + word + " doesn't exists in " + dictionary.getName() +
						  " dictionary. Search was performed using "+ strategy.getDescription() + " strategy.");
	}
	
	/**
	 * Checks word existence in web service output.  
	 * @param dict dictionary in what search was performed.
	 * @param strat strategy which was used for search.
	 * @param word searched word.
	 * @param array web service output. 
	 * @return boolean flag.
	 */
	private boolean checkWordExistenceInTheDictionaryUsingSpecStrategy(Dictionary dict, Strategy strat, String word, 
			   															   ArrayOfDictionaryWord array)
	{
		List<DictionaryWord> dictionaryWords = array.getDictionaryWord();
		for(DictionaryWord wordForPrint: dictionaryWords)
		{
			if(wordForPrint.getWord().equals(word));
			{
				return true;
			}
		}
		return false;
	}
	
}
