package by.epam;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLFileUtil 
{
	
	/**
	 * Parsing xml file for retrieving input words.
	 * @param xmlFilePath XML file path. 
	 * @return       	  Words list from xml file.
	 */
	public static List<String> getInputWords(String xmlFilePath) 
	{
		
		ArrayList<String> inputWords = new ArrayList<String>();
		
		Document doc = null;
		File file = new File(xmlFilePath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			DocumentBuilder builder = dbf.newDocumentBuilder();
			doc = builder.parse(file);
		}
		catch(ParserConfigurationException ex)
		{
			System.out.print("Can't initialize parser! " + ex.getMessage());
		}
		catch(SAXException ex)
		{
			System.out.println("Can't parse xml file! " + ex.getMessage());
		}
		catch(IOException ex)
		{
			System.out.println("Can't find or read xml file! " + ex.getMessage());
		}
		
		if(doc!=null)
		{
			Element documentElement = doc.getDocumentElement(); 
			NodeList wordsNodes = documentElement.getElementsByTagName("Word");
			
			for(int i = 0; i < wordsNodes.getLength(); i++)
			{
				Element wordNode = (Element)wordsNodes.item(i);
				inputWords.add(wordNode.getTextContent());
			}
		}
		
		return inputWords;
	}
	
}
