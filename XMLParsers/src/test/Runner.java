package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import com.sun.org.apache.xerces.internal.impl.XMLStreamReaderImpl;

import sax.SAXHandler;


public class Runner 
{

	private static File xmlFile;
	
	static
	{
		xmlFile = new File("src/shipto.xml");
	}
	
	/**
	 * @param args
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws XMLStreamException 
	 */
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XMLStreamException 
	{
		
//		saxParse();
//		domParse();
//		staxCoursorParse();
		staxIteratorParse();
		
	}

	public static void saxParse() throws SAXException, IOException, ParserConfigurationException
	{
		SAXParserFactory saxFactory = SAXParserFactory.newInstance();
		saxFactory.setNamespaceAware(true);
		SAXParser parser = saxFactory.newSAXParser();
		parser.parse(xmlFile, new SAXHandler());
	}
	
	public static void domParse() throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = domFactory.newDocumentBuilder();
		docBuilder.parse(xmlFile);
	}
	
	public static void staxIteratorParse() throws ParserConfigurationException, SAXException, IOException, XMLStreamException
	{
		XMLInputFactory xmlif = XMLInputFactory.newInstance();
		System.out.println("FACTORY: " + xmlif);
//		xmlif.setEventAllocator(new XMLEventAllocatorImpl());
//		allocator = xmlif.getEventAllocator();
		XMLEventReader xmer = xmlif.createXMLEventReader(new FileInputStream(xmlFile));
		while(xmer.hasNext()){

		    XMLEvent event = xmer.nextEvent();

		    if(event.getEventType() == XMLStreamConstants.START_ELEMENT)
		    {
		        StartElement startElement = event.asStartElement();
		        System.out.println(startElement.getName().getLocalPart());
		    }
		}
	}
	
	public static void staxCoursorParse() throws ParserConfigurationException, SAXException, IOException, XMLStreamException
	{
		XMLStreamReader staxCoursorImpl = new XMLStreamReaderImpl(new FileInputStream(xmlFile), new PropertyManager(1));
		
		int element = staxCoursorImpl.next();
		getEventTypeString(element);
		System.out.println(getEventTypeString(element));
		System.out.println(staxCoursorImpl.getLocalName());
		
//		staxCoursorImpl.getAttributeCount();
		
		while(staxCoursorImpl.hasNext())
		{
			element = staxCoursorImpl.next();
			System.out.println(getEventTypeString(element));
			if(element == XMLEvent.START_ELEMENT)
				System.out.println(staxCoursorImpl.getLocalName());
			
			if(element == XMLEvent.CHARACTERS)
				System.out.println(staxCoursorImpl.getText());
		}
		
		// Write file using StAX
//		XMLStreamWriter writer = new XMLStreamWriterImpl(new FileWriter(new File("src/modified_shipto.xml")), new PropertyManager(2));
//		writer.writeEmptyElement("EmtyTestElement");
//		writer.writeEndDocument();
//		writer.close();
	}
	
	public final static  String getEventTypeString(int  eventType)
	{
	  switch  (eventType)
	    {
	        case XMLEvent.START_ELEMENT:
	          return "START_ELEMENT";
	        case XMLEvent.END_ELEMENT:
	          return "END_ELEMENT";
	        case XMLEvent.PROCESSING_INSTRUCTION:
	          return "PROCESSING_INSTRUCTION";
	        case XMLEvent.CHARACTERS:
	          return "CHARACTERS";
	        case XMLEvent.COMMENT:
	          return "COMMENT";
	        case XMLEvent.START_DOCUMENT:
	          return "START_DOCUMENT";
	        case XMLEvent.END_DOCUMENT:
	          return "END_DOCUMENT";
	        case XMLEvent.ENTITY_REFERENCE:
	          return "ENTITY_REFERENCE";
	        case XMLEvent.ATTRIBUTE:
	          return "ATTRIBUTE";
	        case XMLEvent.DTD:
	          return "DTD";
	        case XMLEvent.CDATA:
	          return "CDATA";
	        case XMLEvent.SPACE:
	          return "SPACE";
	    }
	  return  "UNKNOWN_EVENT_TYPE ,   "+ eventType;
	} 
	
}
