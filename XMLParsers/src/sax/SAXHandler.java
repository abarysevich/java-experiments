package sax;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler 
{
	
	public InputSource resolveEntity (String publicId, String systemId)
	{
		
		return null;
	}
	
	public void startDocument () throws SAXException
	{
	    System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    System.out.println();
	}
	
    public void startElement (String uri, String localName, String qName, Attributes attributes) 
    	throws SAXException
	{
    	StringBuilder fullName = new StringBuilder();
    	fullName.append("<");
//    	if(!"".equals(uri))
//    	{
//    		fullName.append(uri).append(":");
//    	}
    	fullName.append(qName);
    	fullName.append(">");
    	System.out.println(fullName.toString());
	}

	public void endElement (String uri, String localName, String qName) throws SAXException
	{
    	StringBuilder fullName = new StringBuilder();
		fullName.append("</");
    	fullName.append(qName);
    	fullName.append(">");
    	System.out.println(fullName.toString());

	}

	public void characters (char ch[], int start, int length) throws SAXException
	{
    	System.out.println(new String(ch));

	}
	
	
}

