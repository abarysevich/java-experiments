package test;

import game.LifeGame;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		LifeGame game = new LifeGame(4, 4);
		
		game.fillRandomlyGameArea();
		game.printGameArea();
		
		game.incrementGeneration();
		game.printGameArea();
		
		game.incrementGeneration();
		game.printGameArea();
		
		game.incrementGeneration();
		game.printGameArea();
	}

}
