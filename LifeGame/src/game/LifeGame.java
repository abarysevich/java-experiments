package game;

import java.util.Arrays;
import java.util.Random;


public class LifeGame 
{
	
	private boolean[][] m_gameArea;
	private boolean[][] m_newGenerationGameArea;
	private int m_horizontalLength;
	private int m_verticalLength;

	
	public LifeGame(int horizontalLength, int verticalLength)
	{
		m_horizontalLength = horizontalLength;
		m_verticalLength = verticalLength;
		m_gameArea = new boolean[m_horizontalLength][m_verticalLength];
		m_newGenerationGameArea = new boolean[m_horizontalLength][m_verticalLength];
	}
	
	public void fillGameArea(boolean[][] gameArea)
	{
		m_newGenerationGameArea = gameArea;
		gameAreasCopy();
	}
	
	public void fillGameArea()
	{
		for(boolean[] arr: m_newGenerationGameArea)
		{
			Arrays.fill(arr, true);
		}
		gameAreasCopy();
	}
	
	public void fillRandomlyGameArea()
	{
		Random random = new Random();
		for(int i=0; i<m_horizontalLength; i++)
		{
			for(int j=0; j<m_verticalLength; j++)
			{
				m_newGenerationGameArea[i][j] = random.nextBoolean();
			}
		}
		gameAreasCopy();
	}
	
	public void incrementGeneration()
	{
		for(int i=0; i<m_horizontalLength; i++)
		{
			for(int j=0; j<m_verticalLength; j++)
			{
				setCellNextGenerationValue(i, j);
			}
		}
		gameAreasCopy();
	}
	
	public void printGameArea()
	{
//		System.out.println(Arrays.deepToString(m_gameArea));
		for(boolean[] arr: m_gameArea)
		{
			System.out.println(Arrays.toString(arr));
		}
		System.out.println("-----------------------------------------");
	}

//	  Decision maker in accordance to rules above:
//    Any live cell with fewer than two live neighbours dies, as if caused by under-population.
//    Any live cell with two or three live neighbours lives on to the next generation.
//    Any live cell with more than three live neighbours dies, as if by overcrowding.
//    Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
	private void setCellNextGenerationValue(int i, int j)
	{
		byte liveNeighboursCount = getNeighbourLiveCellsCount(i, j);
		if(m_gameArea[i][j])
		{
			setLiveCellNextGenerationValue(i, j, liveNeighboursCount);
		}
		else
		{
			setDeadCellNextGenerationValue(i, j, liveNeighboursCount);
		}
	}
	
	private void setLiveCellNextGenerationValue(int i, int j, byte liveNeighboursCount)
	{
		switch(liveNeighboursCount)
		{
			case 0: case 1: case 4: case 5: case 6: case 7: case 8:
				m_newGenerationGameArea[i][j] = false;
			break;
			
			case 2: case 3:
			  // the same state remained
			  // do nothing	
			break;
			
			default:
				throw new UnsupportedOperationException(String.format("Cell neighbour count is not recognized %d", 
						liveNeighboursCount));
		}
	}
	
	private void setDeadCellNextGenerationValue(int i, int j, byte liveNeighboursCount)
	{
		switch(liveNeighboursCount)
		{
			case 0: case 1: case 2: case 4: case 5: case 6: case 7: case 8:
			  // the same state remained
			  // do nothing	
			break;
			
			case 3:
				m_newGenerationGameArea[i][j] = true;
			break;
			
			default:
				throw new UnsupportedOperationException(String.format("Cell neighbour count is not recognized %d", 
						liveNeighboursCount));
		}
	}
	
	private byte getNeighbourLiveCellsCount(int i, int j)
	{
		byte liveCellsCount = 0;
		for(int tempI = i-1; tempI<=i+1; tempI++)
		{
			if(tempI>=m_horizontalLength || tempI<0)
				continue;
				
			for(int tempJ=j-1; tempJ<=j+1; tempJ++)
			{
				if(tempJ>=m_verticalLength || tempJ<0 || (tempJ==j&&tempI==i))
				   continue;
				
				if(m_gameArea[tempI][tempJ])
					liveCellsCount++;
			}	
		}
		return liveCellsCount;
	}
	
	private void gameAreasCopy()
	{
		for (int i = 0; i < m_horizontalLength; i++) 
		{
             boolean[] member = new boolean[m_verticalLength];
             System.arraycopy(m_newGenerationGameArea[i], 0, member, 0, m_verticalLength);
             m_gameArea[i] = member;
		}
	}
	                  
}

