package test;

import java.rmi.activation.ActivationSystem;
import java.util.Arrays;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
//		Integer[] array = {1,2,3,4,5};
//		shiftArrayToRight(array);
//		printArray(shiftArrayToRight(array, 4));
		
		System.out.print(getFibnacchiNextValue(7));
	}
	
	private static void printArray(Integer[] array)
	{
		System.out.print(Arrays.deepToString(array));
	}
	
	public static int getFibnacchiNextValue(int index)
	{
	    if(index <= 0)
	    return 0;
	    if(index == 1)
	    return 1;
	    return getFibnacchiNextValue(index-1) + getFibnacchiNextValue(index-2);
	}
	
	private static Integer[] shiftArrayToRight(Integer[] array, int shift)
	{
		int arrayLength = array.length;
		shift = shift%arrayLength;
		Integer[] resultArray = new Integer[arrayLength];
		
		System.arraycopy(array, 0, resultArray, shift, arrayLength - shift);
		System.arraycopy(array, arrayLength - shift, resultArray, 0, shift);
		return resultArray;
	}
	
	
	private static void shiftArrayToRight(Integer[] array)
	{
		int arrayLength = array.length; 
		int tempVar = array[0];
		int secondTempVar; 
		
		for(int i=1; i<=arrayLength; i++)
		{
			secondTempVar = array[i%arrayLength];
			array[i%arrayLength] = tempVar;
			tempVar = secondTempVar;
		}
	}
	
}
