package bundle;

import java.util.ListResourceBundle;

public class ItalianBundle_it_IT extends ListResourceBundle {
	@Override
	protected Object[][] getContents() {
		Object bundles[][] = {{"testString", "Italian"}};
		return bundles;
	}
}
