package test;

import java.util.Locale;
import java.util.ResourceBundle;

public class FileResourseBundleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Locale usLocale = new Locale("en", "US"); 
		Locale belarusLocale = new Locale("be", "BY"); 
		Locale singapoureLocale = new Locale("en", "SG"); 

//		Resolving steps:
//		1. Chosen locale
//		2. Default locale
//		3. Default file
		Locale.setDefault(new Locale("sv", "SE"));
		
//		Chooses default locale bundles if exists as ste of resolving
		Locale unknownLocale = new Locale("sk", "SK"); 
											
		ResourceBundle bundle = ResourceBundle.getBundle("bundles.bundle", unknownLocale);
		System.out.print(bundle.getString("testVariable"));
	}

}
