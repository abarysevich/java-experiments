package test;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class FormatTest {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		
		NumberFormat numberFormat = NumberFormat.getInstance(Locale.CHINA);
		numberFormat.setMaximumFractionDigits(3);
		// No rounding!!!!
		System.out.println(numberFormat.format(12345678098765.777777));
		System.out.println(numberFormat.parse("12345678098765L"));
		
		// Currency!!!
		System.out.println(NumberFormat.getCurrencyInstance().format(1000));

		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, Locale.UK);
		System.out.println(dateFormat.format(new Date()));
	}

}
