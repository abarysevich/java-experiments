package test;

import java.util.Locale;
import java.util.ResourceBundle;

public class ListResourseBundleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(ResourceBundle.getBundle(
				"bundle.ItalianBundle", new Locale("it", "IT")).getObject("testString"));
	}

}
