package test;

import java.util.Locale;

public class LocaleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Locale locale = Locale.getDefault();
		System.out.println(locale.getLanguage());
		
		// To get full name, take method with display part!!!
		System.out.println(locale.getDisplayLanguage());
		
		System.out.println(locale.getCountry());
		System.out.println(locale.getDisplayCountry());

		for (Locale localeFromList: Locale.getAvailableLocales()) {
			System.out.println(localeFromList);
		}
	}

}
