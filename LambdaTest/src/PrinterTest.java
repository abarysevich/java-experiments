
public class PrinterTest {

	public interface Printer{
	   void print(String print);
	}
	
	public static class PrintWorker{
	   void doPrintWork(Object obj, Printer printer) {
		   printer.print(obj.toString());
	   }
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Printer samplePrint = p -> System.out.println(p);
		Printer helloPrint = p -> System.out.println("Hello " + p);

		PrintWorker worker = new PrintWorker();
		worker.doPrintWork("Bob", samplePrint);
		worker.doPrintWork("John", helloPrint);
	}

}
