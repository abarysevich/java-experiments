package test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Set;

public class FilesTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Path filePath = Paths.get("file.txt");
		
		PosixFileAttributes attributes = 
				Files.readAttributes(filePath, PosixFileAttributes.class);
		
		// To read permissions - file should exist!!! Otherwise
		// java.nio.file.NoSuchFileException is thrown.
		Set<PosixFilePermission> posixPermissions = attributes.permissions();
		for (PosixFilePermission posixPermission : posixPermissions) {
			System.out.println(posixPermission);
		}

		Path newFilePath = Paths.get("new//directory//file.txt");

		if (!Files.exists(newFilePath)) {
			Files.createDirectories(newFilePath);
		}

		// Possible to copy if all folders in path exist!!!
		Files.copy(filePath, newFilePath, StandardCopyOption.COPY_ATTRIBUTES,
				StandardCopyOption.REPLACE_EXISTING);
		
		Path movedNewFilePath = Paths.get("new//directory2//file.txt");
		if (!Files.exists(newFilePath)) {
			Files.createDirectories(movedNewFilePath);
		}
		Files.move(newFilePath, movedNewFilePath,
				StandardCopyOption.REPLACE_EXISTING);

		Files.delete(movedNewFilePath);
		
		// To delete file it should exist!!!
		Files.delete(movedNewFilePath);
		
	}
}
