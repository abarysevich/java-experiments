package test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class DIrectoryWatchingTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		Path rootDir = Paths.get("new","directory2");
		WatchService watchService = FileSystems.getDefault().newWatchService();
		rootDir.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

		WatchKey watchKey;
		while (true) {
			watchKey = watchService.take();
			processEvenKey(watchKey);
			watchKey.reset();
		}
	}
	
	private static void processEvenKey(WatchKey watchKey) {
		for(WatchEvent<?> event : watchKey.pollEvents()) {
			switch (event.kind().name()) {
				case "OVERFLOW":
					System.out.println("Crap");
				break;
				case "ENTRY_MODIFY":
				   System.out.println(String.format("Entry %s was modified", event.context()));
			}
		}
	}

}
