package test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get(".//file.txt");

		System.out.println(filePath.getFileName());
		System.out.println(filePath.getParent());
		System.out.println(filePath.getRoot());
		System.out.println(filePath.isAbsolute());
		
		Path normalizedPath = filePath.normalize();
		System.out.println(normalizedPath);
		
		System.out.println(filePath.toUri());
		System.out.println(normalizedPath.toUri());
		
		System.out.println(filePath.toAbsolutePath());
		System.out.println(filePath.toAbsolutePath().normalize());

		System.out.println(filePath.toRealPath());

		Path nonExistingFilePath = Paths.get(".//lob//file.txt");
		System.out.println(nonExistingFilePath.toAbsolutePath());
		
		// Should exists on the filesystem!! Throws IOException !!!
//		nonExistingFilePath.toRealPath();
		
		System.out.println(nonExistingFilePath.compareTo(normalizedPath));
	}

}
