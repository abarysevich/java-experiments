package test;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class FileTreeWalkerTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Path rootDir = Paths.get("new");
		Files.walkFileTree(rootDir, new FileVisitor());
	}
	
	private static class FileVisitor implements java.nio.file.FileVisitor<Path> {

		@Override
		public FileVisitResult preVisitDirectory(Path dir,
				BasicFileAttributes attrs) throws IOException {
			System.out.println(String.format("PreVisiting directory: %s",dir.toString()));
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
				throws IOException {
			System.out.println(String.format("Visiting file: %s", file.toString()));
			return FileVisitResult.SKIP_SIBLINGS;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc)
				throws IOException {
			System.out.println(file.toString());
			return FileVisitResult.TERMINATE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc)
				throws IOException {
			System.out.println(String.format("PostVisiting directory: %s",dir.toString()));
			return FileVisitResult.CONTINUE;
		}
		
	}
	
}
