package test;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class ListSystemRootDirectoriesTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileSystem fileSystem = FileSystems.getDefault();
		for(Path rootDirectory: fileSystem.getRootDirectories()) {
			System.out.println(rootDirectory.getFileSystem());
			System.out.println(rootDirectory.toString());
		}
	}

}
