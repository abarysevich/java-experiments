package launcher;

import test.Child;
import test.Parent;
import test.Printer;

public class Starter
{
	
	public static void main(String[] args)
	{
		Printer printer = new Printer();
		printer.print(new Parent());
		printer.print(new Child());
	}
	
}
