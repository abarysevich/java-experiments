package test;

public class Printer 
{
	
	public void print(Parent obj)
	{
		
		System.out.println("obj instanceof Parent " + (obj instanceof Parent));
		System.out.println("obj instanceof Child " + (obj instanceof Child));
		
		Class<?> clazz = obj.getClass();
		//  DDL is working:
		System.out.println(clazz.getCanonicalName());
		obj.printTypeHello();
		
	}
	
}
