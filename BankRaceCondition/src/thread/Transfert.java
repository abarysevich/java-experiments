package thread;

import bank.Bank;

public class Transfert implements Runnable 
{
    
	private Bank m_bank;
	private int m_fromAccount;
	
	public Transfert(Bank bank, int fromAccount)
	{
		m_bank = bank;
		m_fromAccount = fromAccount;
	}
	
	@Override
	public void run() 
	{
		while(true)
		{
			m_bank.transfer(m_fromAccount);
			System.out.printf("Bank total summ after transfert is %d. %n", m_bank.getBankTotalSum() );
		}
	}
	
}
