package runner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import thread.Transfert;

import bank.Bank;
import bank.NotSynchronizedBank;
import bank.synch.block.BlockLockSynchronizedBankWithCondition;
import bank.synch.explicit.ExplicitLockSynchronizedBank;
import bank.synch.explicit.ExplicitLockSynchronizedBankWithCondition;
import bank.synch.implicit.ImplicitLockSynchronizedBank;
import bank.synch.implicit.ImplicitLockSynchronizedBankWithCondition;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		int accountsCount = 3;
		ExecutorService service = Executors.newFixedThreadPool(accountsCount);
//		Bank bank = new NotSynchronizedBank(1000, accountsCount);
//		Bank bank = new ExplicitLockSynchronizedBank(1000, accountsCount);
//		Bank bank = new ImplicitLockSynchronizedBank(1000, accountsCount);
//		Bank bank = new ExplicitLockSynchronizedBankWithCondition(1000, accountsCount);
		Bank bank = new ImplicitLockSynchronizedBankWithCondition(1000, accountsCount);
//		Bank bank = new BlockLockSynchronizedBankWithCondition(1000, accountsCount);
		while(--accountsCount>=0)
		{
			service.submit(new Transfert(bank, accountsCount));
		}
		
	}

}
