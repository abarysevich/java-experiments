package bank;

public abstract class Bank 
{
	
	protected int m_accountsCount;
	protected int[] m_accounts;
	protected int m_transferAmount;
	
	public Bank(int initAccountValue, int accountsCount)
	{
		m_accountsCount = accountsCount;
		m_accounts = new int[m_accountsCount];
		m_transferAmount = initAccountValue/2;
		fillBankInitAccountValues(initAccountValue, accountsCount);
	}
	
	public abstract void transfer(int fromAccount);
	
	public abstract int getBankTotalSum();
	
	private void fillBankInitAccountValues(int initAccountValue, int accountsCount)
	{
		for(int i=0; i<accountsCount; i++)
		{
			m_accounts[i] = initAccountValue; 
		}
	}
	
}
