package bank;

public class NotSynchronizedBank extends Bank 
{
	
	public NotSynchronizedBank(int initAccountValue, int accountsCount)
	{
		super(initAccountValue, accountsCount);
	}
	
	public void transfer(int donorAmountIndex)
	{
		if(m_accounts[donorAmountIndex] < m_transferAmount)
		{
			return;
		}
		int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
		System.out.printf("Tranfer was done from %d account to %d account. Tranfer amount is %d. %n", donorAmountIndex,
				recipientAmountIndex, m_transferAmount);
		m_accounts[donorAmountIndex] -= m_transferAmount;
		m_accounts[recipientAmountIndex] += m_transferAmount;
	}
	
	public int getBankTotalSum()
	{
		int totalSum = 0;
		try
		{
			for(int accountValue: m_accounts)
			{
				totalSum+=accountValue;
				Thread.sleep(100l);
			}
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		return totalSum;
	}
	
}
