package bank.synch.block;

import bank.Bank;



public class BlockLockSynchronizedBankWithCondition extends Bank
{

	// Lock supplier must be the same for every synchronized block with the same data access!!
	// <a>http://docs.oracle.com/javase/tutorial/essential/concurrency/locksync.html</a>
	
	Object m_lockSupplier;  
	
	public BlockLockSynchronizedBankWithCondition(int initAccountValue, int accountsCount) 
	{
		super(initAccountValue, accountsCount);
		m_lockSupplier = new Object();
	}

	@Override
	public void transfer(int donorAmountIndex)
	{
		synchronized (m_lockSupplier) 
		{
			try
			{
				int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
				while(m_accounts[donorAmountIndex] < m_transferAmount)
				{
					System.out.println("Thread was suspended");
					wait();
					System.out.println("Thread was resumed");
				}
				m_accounts[donorAmountIndex] -= m_transferAmount;
				m_accounts[recipientAmountIndex] += m_transferAmount;
				System.out.printf("Tranfer was done from %d account to %d account. Result donor amount is %d; recipient amount is " +
					"%d. %n", donorAmountIndex, recipientAmountIndex, m_accounts[donorAmountIndex], m_accounts[recipientAmountIndex]);
				notifyAll();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public int getBankTotalSum() 
	{
		synchronized (m_lockSupplier) 
		{
			int totalSum = 0;
			try
			{
				for(int accountValue: m_accounts)
				{
					totalSum+=accountValue;
					Thread.sleep(100l);
				}
			}
			catch(InterruptedException ex)
			{
				ex.printStackTrace();
			}
			return totalSum;
		}
	}
	
}
