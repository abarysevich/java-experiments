package bank.synch.implicit;

import bank.Bank;

public class ImplicitLockSynchronizedBankWithCondition extends Bank 
{
	
	public ImplicitLockSynchronizedBankWithCondition(int initAccountValue, int accountsCount)
	{
		super(initAccountValue, accountsCount);
	}

	@Override
	public synchronized void transfer(int donorAmountIndex)
	{
		try
		{
			int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
			while(m_accounts[donorAmountIndex] < m_transferAmount)
			{
				System.out.println("Thread was suspended");
				wait();
				System.out.println("Thread was resumed");
			}
			m_accounts[donorAmountIndex] -= m_transferAmount;
			m_accounts[recipientAmountIndex] += m_transferAmount;
			System.out.printf("Tranfer was done from %d account to %d account. Result donor amount is %d; recipient amount is " +
				"%d. %n", donorAmountIndex, recipientAmountIndex, m_accounts[donorAmountIndex], m_accounts[recipientAmountIndex]);
			notifyAll();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public synchronized int getBankTotalSum() 
	{
		int totalSum = 0;
		try
		{
			for(int accountValue: m_accounts)
			{
				totalSum+=accountValue;
				Thread.sleep(100l);
			}
		}
		catch(InterruptedException ex)
		{
			ex.printStackTrace();
		}
		return totalSum;
	}
	
}
