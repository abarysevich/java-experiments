package bank.synch.implicit;

import bank.Bank;


public class ImplicitLockSynchronizedBank extends Bank
{
	
	public ImplicitLockSynchronizedBank(int initAccountValue, int accountsCount)
	{
		super(initAccountValue, accountsCount);
	}

	/**
	 *     First, it is not possible for two invocations of synchronized methods on the same object to interleave. 
	 *     When one thread is executing a synchronized method for an object, all other threads that invoke synchronized 
	 *     methods for the same object block (suspend execution) until the first thread is done with the object.
     *     Second, when a synchronized method exits, it automatically establishes a happens-before relationship 
     *     with any subsequent invocation of a synchronized method for the same object. 
     *     This guarantees that changes to the state of the object are visible to all threads.
     *     <a>http://docs.oracle.com/javase/tutorial/essential/concurrency/syncmeth.html</a>
	 */
	public synchronized void transfer(int donorAmountIndex)
	{
		if(m_accounts[donorAmountIndex] < m_transferAmount)
		{
			return;
		}
		int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
		System.out.printf("Tranfer was done from %d account to %d account. Tranfer amount is %d. %n", donorAmountIndex,
				recipientAmountIndex, m_transferAmount);
		m_accounts[donorAmountIndex] -= m_transferAmount;
		m_accounts[recipientAmountIndex] += m_transferAmount;
	}
	
	// Race conditions can be seen in case of removing synchronized keyword
	public synchronized int getBankTotalSum()
	{
		int totalSum = 0;
		try
		{
			for(int accountValue: m_accounts)
			{
				totalSum+=accountValue;
				Thread.sleep(100l);
			}
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		return totalSum;
	}
	
}
