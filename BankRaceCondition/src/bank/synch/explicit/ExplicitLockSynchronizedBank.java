package bank.synch.explicit;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import bank.Bank;

public class ExplicitLockSynchronizedBank extends Bank
{
	
	private Lock m_lock;
	
	public ExplicitLockSynchronizedBank(int initAccountValue, int accountsCount)
	{
		super(initAccountValue, accountsCount);
		m_lock = new ReentrantLock();
	}

	@Override
	public void transfer(int donorAmountIndex) 
	{
		m_lock.lock();

		try
		{
			if(m_accounts[donorAmountIndex] < m_transferAmount)
			{
				return;
			}
			int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
			System.out.printf("Tranfer was done from %d account to %d account. Tranfer amount is %d. %n", donorAmountIndex,
					recipientAmountIndex, m_transferAmount);
			m_accounts[donorAmountIndex] -= m_transferAmount;
			m_accounts[recipientAmountIndex] += m_transferAmount;
		}
		finally
		{
			m_lock.unlock();
		}
	}

	// Race conditions can be seen in case of removing m_lock.lock(); line.
	@Override
	public int getBankTotalSum() 
	{
		m_lock.lock();
		int totalSum = 0;
		try
		{
			for(int accountValue: m_accounts)
			{
				totalSum+=accountValue;
				Thread.sleep(100l);
			}
		}
		catch(InterruptedException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			m_lock.unlock();
		}
		return totalSum;
	}
	
	
}
