package bank.synch.explicit;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import bank.Bank;

public class ExplicitLockSynchronizedBankWithCondition extends Bank
{
	
	private Lock m_lock;
	private Condition m_sufficientFunds;
	
	public ExplicitLockSynchronizedBankWithCondition(int initAccountValue, int accountsCount)
	{
		super(initAccountValue, accountsCount);
		m_lock = new ReentrantLock();
		m_sufficientFunds = m_lock.newCondition(); 
	}

	@Override
	public void transfer(int donorAmountIndex)
	{
		m_lock.lock();
		try
		{
			int recipientAmountIndex = (int)(Math.random()*m_accountsCount);
			while(m_accounts[donorAmountIndex] < m_transferAmount)
			{
				System.out.println("Thread was suspended");
				m_sufficientFunds.await();
				System.out.println("Thread was resumed");
			}
			m_accounts[donorAmountIndex] -= m_transferAmount;
			m_accounts[recipientAmountIndex] += m_transferAmount;
			System.out.printf("Tranfer was done from %d account to %d account. Result donor amount is %d; recipient amount is " +
				"%d. %n", donorAmountIndex, recipientAmountIndex, m_accounts[donorAmountIndex], m_accounts[recipientAmountIndex]);
			
			m_sufficientFunds.signalAll();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		finally
		{
			m_lock.unlock();
		}
	}

	@Override
	public int getBankTotalSum() 
	{
		m_lock.lock();
		int totalSum = 0;
		try
		{
			for(int accountValue: m_accounts)
			{
				totalSum+=accountValue;
				Thread.sleep(100l);
			}
		}
		catch(InterruptedException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			m_lock.unlock();
		}
		return totalSum;
	}
	
}
