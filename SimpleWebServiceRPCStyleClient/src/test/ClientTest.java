package test;

import ch01.ts.TimeServerImpl;
import ch01.ts.TimeServerImplService;

public class ClientTest
{
	
	public static void main(String[] args)
	{
		TimeServerImplService serviceFactory = new TimeServerImplService();
		TimeServerImpl impl = serviceFactory.getTimeServerImplPort();
		System.out.print(impl.getTimeAsElapsed());
	}
	
}
