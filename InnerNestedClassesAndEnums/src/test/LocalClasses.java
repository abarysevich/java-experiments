package test;

public class LocalClasses {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	int instanceVariable;
	
	public void method() {
		final int localVariable = 2;

		class Local {
			private void method() {
				// Can access non final instance variable.
 				System.out.println(instanceVariable);
				
				// Can access only local variables which were defined before class:
				int test = localVariable + secondlocalVariable;
			}
		}

		int secondlocalVariable = 2;
	}
}
