package test;

public class InnerClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	private void outerMethod() {
		Inner inner = this.new Inner();
		Inner secondInner = new Inner();
	}
	
	private class Inner {
		private int number = 1;
		
		// Can not have static variables!!
//		private static int secondNumber = 1;

		// Can have constants!!
		private static final int thirdNumber = 1;

	}
}
