package test;

public class NestedClass {

	private int outerNumber;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Nested nested = new Nested();
		
		// Outer class can access inners private variables!!!
		System.out.println(nested.number);
		System.out.println(Nested.staticNumber);
	}

	public static class Nested {
		private int number = 1;
		private static int staticNumber = 2;
		
		private void test() {
			NestedClass outer = new NestedClass();
			System.out.println(outer.outerNumber);
		}
		
	}
	
}
