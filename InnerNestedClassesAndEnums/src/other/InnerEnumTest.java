package other;

import test.InnerEnums;

public class InnerEnumTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Inner enums implicitly static and final !!!
		InnerEnums.A a = InnerEnums.A.a;
		InnerEnums.B b = InnerEnums.B.b;
		
	}

}
