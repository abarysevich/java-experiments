package test;

public class Test 
{
	public static Test instance = new Test();
	
	public static int variable = 3;

	private static final int CONST = 2;
	
	private int var;
	
	public Test()
	{
		var = variable + CONST;
	}
	
	public int getVariable()
	{
		return var;
	}
}
