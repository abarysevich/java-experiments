package test.beans;

public interface ITestBean 
{

	void setProperty(String property);

	String getProperty();

	String getTestString();

	String getSecondTestString();
}