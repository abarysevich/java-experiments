package test;

import java.lang.reflect.InvocationTargetException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import test.beans.ITestBean;

public class ProxyLauncher {

	/**
	 * @param args
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException 
	{
		
		ApplicationContext context = new ClassPathXmlApplicationContext("ioc-context.xml");
		ITestBean bean = (ITestBean)context.getBean("testBean");
		
		System.out.println(bean.getTestString());
		
		System.out.println(bean.getSecondTestString());
	}
	
}