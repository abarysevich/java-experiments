package test;

import java.lang.reflect.InvocationTargetException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import test.beans.ITestBean;

public class AutoProxyLauncher {

	/**
	 * @param args
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException 
	{
		
		ApplicationContext context = new ClassPathXmlApplicationContext("ioc-context-autoproxy.xml");
		ITestBean bean = (ITestBean)context.getBean("test.beans.TestBean");
		
		System.out.println(bean.getTestString());
		
		System.out.println(bean.getSecondTestString());
		
	}
	
}