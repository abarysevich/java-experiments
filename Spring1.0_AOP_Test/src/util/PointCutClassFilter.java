package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.aop.ClassFilter;

public class PointCutClassFilter implements ClassFilter {

	private Pattern classPattern;
	
	public PointCutClassFilter(String classPattern)
	{
		this.classPattern = Pattern.compile(classPattern);
	}
	
	public boolean matches(Class<?> clazz) 
	{
		Matcher matcher = classPattern.matcher(clazz.getName());
		return matcher.matches();
	}
	
}
