package test;

import messanger.QueueMessenger;
import messanger.TopicMessager;

import com.sun.messaging.Queue;
import com.sun.messaging.Topic;


public class Runner 
{
	
	public static void main(String[] args) throws Exception
	{
		Queue queue = new Queue("World");
		QueueMessenger messenger = new QueueMessenger();
		messenger.sendMessage(queue);
		messenger.recieveMessage(queue);
		messenger.closeSession();
		
		Topic topic = new Topic("Topic");
		TopicMessager topicMess = new TopicMessager();
		topicMess.sendMessage(topic);
		topicMess.recieveMessage(topic);
		topicMess.closeSession();
		
	}
	
}
