package messanger;

import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

public class TopicMessager extends JMSMessenger 
{

	public TopicMessager() throws Exception 
	{
		super();
	}

	@Override
	public void sendMessage(Destination destination) throws Exception 
	{
		MessageProducer producer = session.createProducer(destination);
		TextMessage textMessage = session.createTextMessage();
		textMessage.setText("Text message");
		producer.send(textMessage);
	}

	@Override
	public void recieveMessage(Destination destination) throws Exception 
	{
		conn.start();
		MessageConsumer receiver = session.createConsumer(destination);
		Message message = receiver.receive();
		if(message instanceof TextMessage)
		{
			System.out.println(((TextMessage)message).getText());
		}
	}

}
