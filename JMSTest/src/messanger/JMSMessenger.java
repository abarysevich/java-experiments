package messanger;

import javax.jms.Destination;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;

public abstract class JMSMessenger 
{
	
	QueueSession session;
	QueueConnection conn;

	public JMSMessenger() throws Exception
	{
		QueueConnectionFactory connFactory = new com.sun.messaging.QueueConnectionFactory();
		conn = connFactory.createQueueConnection();
		session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	}
	
	public abstract void sendMessage(Destination destination) throws Exception;
	
	public abstract void recieveMessage(Destination destination) throws Exception;
	
	public void closeSession() throws Exception
	{
		session.close();
		conn.close();
	}
	
}
