package messanger;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.TextMessage;


public class QueueMessenger extends JMSMessenger 
{
	
	public QueueMessenger() throws Exception
	{
		super();
	}

	@Override
	public void sendMessage(Destination senderQueue) throws Exception 
	{
		QueueSender sender = session.createSender((Queue)senderQueue);
		TextMessage textMessage = session.createTextMessage();
		textMessage.setText("Text message");
		sender.send(textMessage);
	}

	@Override
	public void recieveMessage(Destination senderQueue) throws Exception 
	{
		conn.start();
		QueueReceiver receiver = session.createReceiver((Queue)senderQueue);
		Message message = receiver.receive();
		if(message instanceof TextMessage)
		{
			System.out.println(((TextMessage)message).getText());
		}
	}
	
	public void receiveMessageAsynch(Queue senderQueue) throws Exception
	{
		conn.start();
		QueueReceiver receiver = session.createReceiver(senderQueue);
		receiver.setMessageListener(new MessageListener() 
			{
				@Override
				public void onMessage(Message message) 
				{
					if(message instanceof TextMessage)
					{
						try 
						{
							System.out.println(((TextMessage)message).getText());
						}
						catch (JMSException e) 
						{
							e.printStackTrace();
						}
					}
				}
			});
		

		
	}
 
}
