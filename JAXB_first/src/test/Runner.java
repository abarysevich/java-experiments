package test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import withoutxmlroot.ObjectFactory;
import withoutxmlroot.ShiporderType;
import withxmlroot.Shiporder;

public class Runner 
{
	public static void main(String[] args) throws JAXBException
	{
		
//		WithXmlRootElement
		JAXBContext withXmlRootContext = JAXBContext.newInstance(Shiporder.class);
		Marshaller withXmlRootMarsh =  withXmlRootContext.createMarshaller();
		withXmlRootMarsh.marshal(new Shiporder(), System.out);
		
//		WithoutXmlRootElement
		
//		Schiporder type in schema can used by multiple different tag names
//		That is why creation of JAXBElement is required.
//		see: http://weblogs.java.net/blog/2006/03/03/why-does-jaxb-put-xmlrootelement-sometimes-not-always
		
		JAXBContext withoutXmlContext = JAXBContext.newInstance(ShiporderType.class);
		Marshaller witoutMarshaller =  withoutXmlContext.createMarshaller();
		
		JAXBElement<ShiporderType> object = new JAXBElement<ShiporderType>(
				new QName("", "RootElement"), ShiporderType.class, new ShiporderType());
		witoutMarshaller.marshal(object, System.out);
		
		ObjectFactory of = new ObjectFactory();
		JAXBElement<ShiporderType> secondObject = of.createShiporder(new ShiporderType()); 
		witoutMarshaller.marshal(secondObject, System.out);
		
	}
}
