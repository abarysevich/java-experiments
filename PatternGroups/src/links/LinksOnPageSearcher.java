package links;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class LinksOnPageSearcher 
{
	private static final String pageURL = "http://tut.by";
	
	
	public static void main(String[] args) throws IOException 
	{
		String pageSource = getPageSourse(pageURL);
		System.out.println(pageSource);
	}
	
	private static String getPageSourse(String pageURL) throws IOException
	{
		InputStream pageSource = getPageSource(pageURL);
		return getStringPageSourse(pageSource);
	}
	
	private static String getStringPageSourse(InputStream page) throws IOException
	{
		InputStreamReader reader = new InputStreamReader(page);
		StringBuilder pageString = new StringBuilder();  
		int pageChar = reader.read();
		while(pageChar != -1)
		{
			pageString.append((char)pageChar);
			pageChar = reader.read();
		}
		page.close();
		return pageString.toString();
	}
	
	private static InputStream getPageSource(String page) 
	{
		try
		{
			URL pageURL = new URL(page);
			URLConnection conn = pageURL.openConnection();
			return conn.getInputStream();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
