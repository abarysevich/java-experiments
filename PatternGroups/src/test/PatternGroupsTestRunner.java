package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternGroupsTestRunner 
{
	
	static Pattern pattern = Pattern.compile("(\\d\\d)(\\w\\w)");
	
	
	public static void main(String[] args) 
	{
		Matcher matcher = pattern.matcher("353s");
		
		boolean foumd = matcher.find();
		if(foumd)
		{
			for(int i=0; i < matcher.groupCount(); i++)
			{
		        String groupStr = matcher.group(i);
		        System.out.println(groupStr); 
			}
		}
		else
		{
			System.out.println("Nothing has been found.");
			System.out.println(matcher.groupCount());
		}
		System.out.println(matcher.group(0));
	}
	
	
	
}
