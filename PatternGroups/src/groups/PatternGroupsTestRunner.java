package groups;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternGroupsTestRunner 
{
	private static Pattern pattern = Pattern.compile(".*[A-Z][a-z]*([A-Z][a-z]*)+.*");
	
	public static void main(String[] args) 
	{
		String input = "Here is a WikiWord followed by AnotherWikiWord, then SomeWikiWordtoretrieve.";
		int groupNumber = 1;
		System.out.println(String.format("Substring captured be group %d is: %s", groupNumber,
				printStringCapturedByGroup(groupNumber, input)));
	}
	
	private static String printStringCapturedByGroup(int groupNumber, String inputString)
	{
		Matcher matcher = pattern.matcher(inputString);
		if(matcher.matches()&&(matcher.groupCount()>0))
		{
			return matcher.group(groupNumber);
		}
		return null;
	}
	
	
}
