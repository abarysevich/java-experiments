package test;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		weakReferenceTest();
		softReferenceTest();
	}

	private static void softReferenceTest() {
		Test newInstance = new Test();
		SoftReference<List<String>> sofReference =
				new SoftReference<>(newInstance.fillListWithData(new ArrayList<String>()));
		System.out.println(sofReference.get()==null);
		List<String> strings = new ArrayList<>();
		newInstance.fillListWithMoreData(strings);
		System.out.println(sofReference.get()==null);
	}
	
	private static void weakReferenceTest() {
		Test newInstance = new Test();
		WeakReference<Object> weakReference = newInstance.getWeakReference();
		System.out.println(weakReference.get());
		List<String> strings = new ArrayList<>();
		newInstance.fillListWithData(strings);
		System.out.println(weakReference.get());
	}

	private List<String> fillListWithData(List<String> strings) {
		String start = "string_";
		for (int i = 1_000_000; i > 0; i--) {
			strings.add(start + i);
		}
		return strings;
 	}

	private List<String> fillListWithMoreData(List<String> strings) {
		String start = "string_";
		for (int i = 5_000_000; i > 0; i--) {
			strings.add(start + i);
		}
		return strings;
 	}
	
	private WeakReference<Object> getWeakReference() {
		return new WeakReference<Object>(new Test());
	}
	
	public String toString() {
		return "hello";
	}
}

