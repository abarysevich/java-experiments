package test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueInterrupter {

	public static void main(String[] args) {
		Thread runner = new Thread(new HardWorker());
		runner.start();
		runner.interrupt();
	}

	private static final class HardWorker implements Runnable {
		private BlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<Object>(5000);
		@Override
		public void run() {
			while (true) {
				try {
					// It is not required to be in blocked state for InterruptedException
					// throwing. If method checks (in this case BlockingQueue.put(Object obj))
					// that thread was interrupted it throws InterruptedException.  This is 
					// interrupted flag handling one of the possible cases. 
					
					blockingQueue.put(new Object());
					System.out.println(blockingQueue.size());
				} catch (InterruptedException e) {
					System.out.println("Thread was interrupted");
					System.out.println(String.format(
						"Thread interrupt flag after InterruptedException was thrown is %b", 
						Thread.currentThread().isInterrupted()));
					break;
				}
			}
			
		}
		
	}
}
