package test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ThreadInterrupter {

	public static void main(String[] args) {
		Thread runner = new Thread(new HardWorker());
		runner.start();
		runner.interrupt();
	}

	private static final class HardWorker implements Runnable {
		private BlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<Object>(2);
		@Override
		public void run() {
			while (true) {
				try {
					blockingQueue.put(new Object());
				} catch (InterruptedException e) {
					System.out.println("Thread was interrupted");
					System.out.println(String.format(
						"Thread interrupt flag after InterruptedException was thrown is %b", 
						Thread.currentThread().isInterrupted()));
					break;
				}
			}
			
		}
		
	}
}
