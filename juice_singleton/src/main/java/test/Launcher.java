package test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import entities.FirstDependency;
import entities.SecondDependency;

public class Launcher {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new TestModule());

        FirstDependency firstFirst = injector.getInstance(FirstDependency.class);
        System.out.println(firstFirst.getGuid());

        FirstDependency secondFirst = injector.getInstance(FirstDependency.class);
        System.out.println(secondFirst.getGuid());

        SecondDependency firstSecond = injector.getInstance(SecondDependency.class);
        System.out.println(firstSecond.getGuid());

        SecondDependency secondSecond = injector.getInstance(SecondDependency.class);
        System.out.println(secondSecond.getGuid());
    }

}
