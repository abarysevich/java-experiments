package test;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import entities.DependencyImpl;
import entities.FirstDependency;
import entities.SecondDependency;

public class TestModule extends AbstractModule {
    protected void configure() {
        bind(FirstDependency.class).to(DependencyImpl.class);
        // Only keeps it as a singleton for one mapping. Still creates separate instances for
        // the mappings to different types.
        bind(SecondDependency.class).to(DependencyImpl.class).in(Singleton.class);
    }
}
