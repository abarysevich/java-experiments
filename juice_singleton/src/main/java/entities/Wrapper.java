package entities;

import com.google.inject.Inject;

public class Wrapper {

    private DependencyImpl dependency;

    @Inject
    public Wrapper(DependencyImpl dependency) {
        this.dependency = dependency;
    }

    public void printDependencyGuid() {
        System.out.println(dependency.getGuid());
    }
}
