package entities;


import com.google.inject.Singleton;

import java.util.Random;

// Only makes it real singleton.
@Singleton
public class DependencyImpl implements FirstDependency, SecondDependency {
    private final int GUID = new Random().nextInt();

    public int getGuid() {
        return GUID;
    }
}
