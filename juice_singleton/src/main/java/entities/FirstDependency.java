package entities;

/**
 * Created by abarysevich on 4/21/2018.
 */
public interface FirstDependency {
    int getGuid();
}
