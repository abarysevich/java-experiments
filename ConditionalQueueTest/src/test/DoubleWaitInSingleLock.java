package test;

public class DoubleWaitInSingleLock {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final ObjectWithState objectWithState = new ObjectWithState();
		Thread changer = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					objectWithState.change();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}
		});
		changer.start();
		
		objectWithState.printCurrentState();
		objectWithState.allowToChangeFirstTime();
		objectWithState.printCurrentState();
		
		Thread alloewanceChanger = new Thread(new Runnable() {
			@Override
			public void run() {
				objectWithState.allowToChangeSecondTime();
			}
		});
		alloewanceChanger.start();
		objectWithState.printCurrentState();
	}
	
	private static final class ObjectWithState {
		private volatile boolean notAllowedToChangeFirstTime = true;
		private volatile boolean notAllowedToChangeSecondTime = true;
		private StringBuilder state = new StringBuilder("Initialized ");
		
		public synchronized void change() throws InterruptedException {
			while(notAllowedToChangeFirstTime) {
				wait();
			}
			state.append("changed first time");
			while(notAllowedToChangeSecondTime) {
				wait();
			}
			state.append("changed second time");
		}
		
		public synchronized void allowToChangeFirstTime() {
			notAllowedToChangeFirstTime = false;
			notify();
		}
		
		public synchronized void allowToChangeSecondTime() {
			notAllowedToChangeSecondTime = false;
			notify();
		}
		
		public void printCurrentState() {
			System.out.println(state.toString());
		}
	}

}
