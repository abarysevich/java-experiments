package test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NotifySignalSharingBetweenTwoThreads {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		final ObjectWithState firstObjectWithState = new ObjectWithState("First object");
		Thread runner = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					firstObjectWithState.modifyState();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		runner.start();
		Thread.sleep(100);
		ObjectWithState secondObjectWithState = new ObjectWithState("Second object");
		
		
		// Condition.signallAll() awakes only threads who were suspended by the same lock as was
		// used for Condition object creation. Same is for insintric lock notifyAll() awakes only
		// threads which were suspended by in the same lock as used when notifyAll is called.
		secondObjectWithState.allowModification();
//		firstObjectWithState.allowModification();
	}

	private static final class ObjectWithState {
		private volatile boolean notAllowedToModifyState = true;
		private Lock lock = new ReentrantLock();
		private Condition condition = lock.newCondition();
		private String name;
		
		public ObjectWithState(String name) {
			this.name = name;
		}

		public void modifyState() throws InterruptedException {
			try {
				lock.lock();
				while (notAllowedToModifyState) {
					condition.await();
					System.out.println(
							String.format("Notify signal wake up waiting thread for object %s", name));
				}
			}
			finally {
				lock.unlock();
			}
		}
		
		public void allowModification() {
			try {
				lock.lock();
				notAllowedToModifyState = false;
				condition.signalAll();
			}
			finally {
				lock.unlock();
			}
		}
	}
}
