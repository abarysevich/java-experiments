package test;

public class WaitNotifyCallWithoutLock {
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		WaitNotifyCallWithoutLock object = new WaitNotifyCallWithoutLock();
//		java.lang.IllegalMonitorStateException result of below methods call:
		object.wait();
//		object.notify();
	}
}
