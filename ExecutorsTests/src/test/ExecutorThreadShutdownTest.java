package test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorThreadShutdownTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
//				try {
					while(true) {
//						Thread.sleep(1000);
						System.out.println(
								String.format("Current interrupted status value is %b", 
								Thread.currentThread().isInterrupted()));
						System.out.println("Inside while loop.");
					}
//				} catch (InterruptedException e) {
//					System.out.println("Thread was interrupted while sleeping");
//					System.out.println(String.format("Interrupt status after exception is %b", 
//							Thread.currentThread().isInterrupted()));
//					Thread.currentThread().interrupt();
//				}
//				System.out.println("Thread was interrupted and ascaped loop!");
			}
		});
		executor.submit(thread);
		executor.shutdownNow();
	}

}
