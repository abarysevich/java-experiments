package test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExceptionDuringExecutionTest {
	public static void main(String[] args) {
		Runnable runnableTask = new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(10000L);
					System.out.println("Inside thread");
					throw new RuntimeException();
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.out.println(e.getLocalizedMessage());
				} catch (RuntimeException ex) {
					System.out.println("Message was caught");
					throw ex;
				}
			}
		}; 
//		Thread testThread = new Thread(runnableTask);
//		testThread.start();
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(runnableTask);
		executor.shutdown();
	}
}
