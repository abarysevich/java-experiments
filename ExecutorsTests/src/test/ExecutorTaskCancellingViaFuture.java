package test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorTaskCancellingViaFuture {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Runnable task = new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted()) {
					System.out.println("Inside the thread");
				}
			}
		};
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<?> future = executor.submit(task);
		future.cancel(true);
		executor.shutdown();
	}

}
