package test;

import java.util.concurrent.CountDownLatch;

import structure.BoundedList;

public class Runner {
	private final static BoundedList boundedList = new BoundedList((byte)10);
	private static final CountDownLatch lathce = new CountDownLatch(1);
	
	public static void main(String[] args) {
		Thread adder = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					try {
						lathce.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					boundedList.add(new Object());
				}
			}
		});
		
		Thread remover = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					try {
						lathce.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					boundedList.removeObject();
				}
			}
		});
		adder.start();
		remover.start();
		lathce.countDown();
	}    
}
