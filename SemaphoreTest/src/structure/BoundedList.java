package structure;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class BoundedList {
	List<Object> list = Collections.synchronizedList(new LinkedList<>());
	Semaphore semaphore;

	public BoundedList(byte bound) {
		semaphore = new Semaphore(bound);
	}

	public void add(Object object) {
		semaphore.acquireUninterruptibly();
		boolean result = list.add(object);
		if (!result) {
			semaphore.release();
		} else {
			System.out.println(
					String.format("Object was added. Now list size is %d", list.size()));
		}
	}
	
	public void removeObject() {
		int size = list.size();
		if (size > 0) {
			Object removedObject = list.remove(--size);
			if (removedObject != null) {
				System.out.println(
					String.format("Object on the next position %d was deleted", size));
				semaphore.release();
			}
		}
	}
}