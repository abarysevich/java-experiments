package test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionWideningTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	interface A {
		void abc() throws IOException;
	}
	
	interface B {
		void abc() throws FileNotFoundException;
	}
	
	class C implements A, B {
		
		// Can not widen exception type from B interface
		// but can narrow eception type from A interface
		public void abc() throws FileNotFoundException {
			// TODO Auto-generated method stub
			
		}
		
	}
}
