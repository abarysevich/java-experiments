package test;

import java.util.ArrayList;
import java.util.List;

public class GenericWithRawTypes {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Possible when raw type from the right!!!
		List<String> list = new ArrayList();
		list.add("hello");
		
		// Possible when raw type from the left!!!
		List secondList = new ArrayList<String>();
		secondList.add("hello");		
	}

}
