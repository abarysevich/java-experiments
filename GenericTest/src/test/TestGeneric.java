package test;

import java.util.ArrayList;
import java.util.List;

public class TestGeneric <T> 
{
	
	private void method(Object arg)
	{
//		Results of erasure: 
		
//		 Next action is forbidden because type info is erased on runtime and all this actions
//		 are expected to have type info during runtime.
//  	 T treated as Object type on runtime.
//		 T object = new T();
//		 if(arg instanceof T)
//		 T[] array = new T[1]
		
//		 Next action is not forbidden because on the compile time compiler adds type casts
//		using info about T which is still exists (is deleted after compiliing)!!!
		List<T> list = new ArrayList<T>();	
	}
	
	/**
	 * Handles list with N types elements inside.
	 * @param list
	 */
	private <N> void doSomething(List<N> list) {
		list.add(list.get(0));
	}
	
	/**
	 * Handles list with unknown types elements inside.
	 * @param list
	 */
	private void doSomethingWildcard(List<?> list) {
//		Not possible because type is unknown!!!
//		list.add(list.get(0));
		
//		Only universal type null adding is possible!!! 
		list.add(null);
	}
}
