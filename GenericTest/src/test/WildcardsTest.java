package test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WildcardsTest 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
		List<String> testList = new ArrayList<String>();
		
//		POLYMORFISM ISN'T SUPPORTED  !!!!!!!!!!!!
//		List<String> testListError = new ArrayList<Object>();
		
		testList.add("a");
		testList.add("b");
		
		listPrint(testList);
		
		List<Date> dateList = new ArrayList<Date>();
		dateList.add(new Date());
		
		listPrint(dateList);
		
//		Forbidden instantiation when question on right side:
//		List<?> populatedWildcardError = new ArrayList<?>();
		List<?> populatedWildcard = new ArrayList<Object>();
		
//		Forbidden any objects except null add 
//		populatedWildcard.add("");
//		populatedWildcard.add(new Date());
		populatedWildcard.add(null);
	}
	
	private static void listPrint(List<?> list)
	{
		for(Object obj: list)
		{
			System.out.println(obj.toString());
		}
	}
	
	private static void listaddPrint(List<? super String> list)
	{
		list.add("hello");
	}
	
//	private static void listPrint(List<Object> list)
//	{
//		for(Object obj: list)
//		{
//			System.out.println(obj.toString());
//		}
//	}
	
}
