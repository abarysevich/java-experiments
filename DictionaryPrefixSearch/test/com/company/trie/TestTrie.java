package com.company.trie;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class TestTrie
{
    @Test
    public void testAdd()
    {
        Trie trie = new Trie();
        trie.add("test");

        assertTrue(trie.search("test"));
    }

    @Test
    public void testCheck()
    {

    }
}
