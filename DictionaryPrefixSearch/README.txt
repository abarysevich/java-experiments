Duetto Java Programming Test

Goal:
    Create a dictionary that can be built from a text file and is searchable based on the start of the word.
    
Tasks:
    - Read in \resources\words.txt which contains over 230,000 words separated by newlines to populate the 
      dictionary.
    - File IO should be implemented in main.file.DictionaryReader and should support the method for filling 
      a dictionary.
    - Design and implement a data structure for the dictionary that is both memory efficient in storing all 
      words and fast in searching words by prefix.
    - Given a prefix such as "aberran", the dictionary should return "aberrance", "aberrancy", and "aberrant".
    - If a prefix is a complete word, that word should also be returned.
    - Interactions with the dictionary will be through main.dictionary.Dictionary which must also be 
      implemented.
    - main.dictionary.Dictionary must be a thread safe singleton given the following assumptions:
        - All words are added to the dictionary before being read.
        - Words are only added by a single thread.
        - Words can be searched for by multiple concurrent threads.
    - main.Console is provided to test the dictionary.
    - Implement a dictionary cache that caches the last dictionary search until memory becomes constrained. 
      If the cache is cleared due to memory constraints, the search should just be performed again.
    
Guidelines:
    - Provide comments to let us know why you chose a particular data structure or algorithm or made any
      particular assumptions.
    - You may use the internet for javadocs or any other reference material, but do not use any code found
      on the internet. You may only use the classes and libraries that comes with the default JDK.