package com.company;

import com.company.dictionary.Dictionary;
import com.company.dictionary.tree.TreeDictionary.DictionarySingletonHolder;
import com.company.file.DictionaryReader;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
            throws IOException
    {

        Dictionary dict = DictionarySingletonHolder.INSTANCE;
        //fill the dictionary from words.txt
        DictionaryReader reader = new DictionaryReader("resources/words.txt");
        reader.fillDictionary(dict);

        //wait for input
        Scanner in = new Scanner(System.in);
        while (true) {
            String prefix = in.nextLine();

            //if input is "exit", quit, else return all words that start with or matches the prefix
            if (prefix.equals("EXIT")) {
                break;
            }
            else {
                List<String> words = dict.getWords(prefix);
                if (words != null) {
                    for (String word : words) {
                        System.out.println(" - " + word);
                    }
                }
                else {
                    System.out.println(" - NO RESULTS");
                }
            }
        }
        in.close();
    }

}
