package com.company.trie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Trie
{
    static final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-";
    Node root = new Node();

    public List<String> getWords(String prefix)
    {
        Node startNode = getNode(prefix);
        if (startNode == null) {
            return Collections.EMPTY_LIST;
        }

        return collectWords(startNode, prefix);
    }

    private List<String> collectWords(Node startNode, String prefix)
    {
        List<String> result = new ArrayList<>();
        collectWords(startNode, prefix, result);
        return result;
    }

    private void collectWords(Node currentNode, String prefix, List<String> result)
    {
        if (currentNode == null) {
            return;
        }

        if (currentNode.value != null) {
            result.add(prefix);
        }

        for (int alphabetIndex = 0; alphabetIndex < alphabet.length(); alphabetIndex++) {
            collectWords(currentNode.next[alphabetIndex], prefix + alphabet.charAt(alphabetIndex),
                    result);
        }
    }

    private Node getNode(String word)
    {
        return getNode(word, 0, root);
    }

    private Node getNode(String word, int index, Node current)
    {
        if (current == null) {
            return null;
        }

        if (index >= word.length()) {
            return current;
        }

        return getNode(word, index + 1, current.next[alphabet.indexOf(word.charAt(index))]);
    }

    public boolean search(String word)
    {
        return search(0, word, root);
    }

    private boolean search(int index, String word, Node current)
    {
        if (current == null) {
            // miss
            return false;
        }

        if (current.value != null && index == word.length()) {
            // hit
            return true;
        }

        if (index >= word.length()) {
            // miss
            return false;
        }

        char nextChar = word.charAt(index);

        if (nextChar == '.') {
            for (Node nextNode : current.next) {
                if (search(index + 1, word, nextNode)) {
                    return true;
                }
            }
            return false;
        }
        else {
            Node nextNode = current.next[alphabet.indexOf(word.charAt(index))];
            return search(index + 1, word, nextNode);
        }
    }

    public void add(String word)
    {
        add(0, word, root);
    }

    private void add(int index, String word, Node current)
    {
        if (index >= word.length()) {
            return;
        }

        Node nextCharNode = current.next[alphabet.indexOf(word.charAt(index))];
        if (nextCharNode == null) {
            nextCharNode = new Node();
            if (index == word.length() - 1) {
                nextCharNode.value = new Integer(1);
            }
            current.next[alphabet.indexOf(word.charAt(index))] = nextCharNode;
        }
        add(++index, word, nextCharNode);
    }

    static class Node
    {
        Integer value;
        Node[] next = new Node[53];
    }
}

