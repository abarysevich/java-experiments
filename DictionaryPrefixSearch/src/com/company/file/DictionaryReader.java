package com.company.file;

import com.company.dictionary.Dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A file reader that reads a list of newline delimited words into a Dictionary.
 */
public class DictionaryReader
{
    private final String distionaryFilePath;

    /**
     * Create a new dictionary reader for the file at fileLoc.
     */
    public DictionaryReader(String fileLoc)
    {
        this.distionaryFilePath = fileLoc;
    }

    /**
     * Fill the Dictionary with all words in the file.
     */
    public void fillDictionary(Dictionary dictionary)
    {
        if (dictionary == null) {
            throw new IllegalArgumentException("Dictionary can not be null!");
        }
        try (Scanner fileScanner = new Scanner(new File(this.distionaryFilePath))) {
            while (fileScanner.hasNextLine()) {
                String dictionaryEntry = fileScanner.nextLine().trim();
                if (!"".equals(dictionaryEntry)) {
                    dictionary.addWord(dictionaryEntry);
                }
            }
        }
        catch (FileNotFoundException e) {
            System.err.printf("Exception while reading the dictionary file: %s",
                    e.getLocalizedMessage());
        }
    }
}
