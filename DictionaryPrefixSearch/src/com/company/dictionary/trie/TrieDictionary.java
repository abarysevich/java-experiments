package com.company.dictionary.trie;

import com.company.dictionary.Dictionary;
import com.company.trie.Trie;

import java.util.List;

// Requires ~200 MB of memory to hold a trie for all dictionary Strings - search is O(1)
// 53 [alphabet size] ^ 5 [average word size] bytes.
public class TrieDictionary implements Dictionary
{
    private Trie dictionaryTrie = new Trie();

    @Override
    public void addWord(String word)
    {
        dictionaryTrie.add(word);
    }

    @Override
    public List<String> getWords(String prefix)
    {
        return dictionaryTrie.getWords(prefix);
    }

    // Auxiliary class that gives warranty that the only one Dictionary instance is possible
    // in the multithread environment.
    public static class DictionarySingletonHolder {
        public static final Dictionary INSTANCE = new TrieDictionary();
    }
}
