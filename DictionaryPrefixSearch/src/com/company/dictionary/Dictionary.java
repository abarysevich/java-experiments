package com.company.dictionary;

import java.util.List;

public interface Dictionary
{
    void addWord(String word);

    List<String> getWords(String prefix);
}
