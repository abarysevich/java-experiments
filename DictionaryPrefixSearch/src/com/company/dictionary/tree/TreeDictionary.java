package com.company.dictionary.tree;

import com.company.dictionary.Dictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * A thread-safe concurrent Dictionary singleton that allows for adding words and
 * searching words by prefix. For more information on assumptions on concurrency
 * see the README.
 *
 *  Requires ~25 MB of memory to hold all the dictionary Strings in the TreeSet - search is O(1)
 */
public class TreeDictionary implements Dictionary
{
    // This data structure allows to make efficient search O(logN) and keep all unique
    // dictionary items in memory
    private final TreeSet<String> words = new TreeSet<>();
    private final Runtime runtime = Runtime.getRuntime();
    private List<String> lastSearchResult;
    private String lastSearchPrefics;

    private TreeDictionary(){
        // Only required to prevent object instantiation from outside. Left empty.
    }

    /**
     * Return the shared Dictionary singleton.
     * @return
     */
    public static Dictionary getInstance() {
        // Auxiliary class is loaded only during the first getInstance() method call. That allows
        // to use lazy instantiation pattern.
        return DictionarySingletonHolder.INSTANCE;
    }

    //  Race condition doesn't exists since only single thread can add words using
    //  this method and all worlds should be added before words search
    //  that means that this method doesn't require any synchronization.
    /**
     * Add a word to the current dictionary.
     * @param word
     */
    @Override
    public void addWord(String word) {
        if (word == null || "".equals(word)) {
            throw new IllegalArgumentException("String can not be null or empty!");
        }
        words.add(word);
    }

    /**
     * Get all words that start with or matches prefix.
     * @param prefix
     * @return
     */
    @Override
    public List<String> getWords(String prefix) {
        if (prefix == null || "".equals(prefix)) {
            throw new IllegalArgumentException("Prefix can not be null or empty!");
        }

        if (prefix.equals(this.lastSearchPrefics) && this.lastSearchResult != null) {
            return lastSearchResult;
        }

        List<String> searchResult = new ArrayList<>();
        // Data modification is absent. No synchronization is required!
        // based on the lexicographic comparison.
        Set<String> wordsCandidates = words.tailSet(prefix, true);
        for (String candidate: wordsCandidates) {
            if(candidate.startsWith(prefix)) {
                searchResult.add(candidate);
            } else {
                // wordsCandidates Set view is also NavigableSet - words iteration in this set
                // is in alphabetic order, that means that after first word that starts not from
                // target prefix is hit, there is no more words in this subset that start from
                // target prefix, no more traversing is required.
                break;
            }
        }

        // Section of code below should be synchronized. Two variables lastSearchResult and lastSearchPrefics
        // should change action should be atomic.
        // Thread should check memory state and should have warranty that there is no other thread can
        // modify cashed lastSearchResult before current thread modifies it.
        synchronized (this) {
            lastSearchPrefics = prefix;
            // Memory check here. If there is not enough memory (less than 20% of elidgeble JVM memory is free)
            // attempt to get more free memory by nullifying cashed previous search - if it doesn't help,
            // current search value is not cashed at all
            if (isSufficientMemoryAmount()) {
                lastSearchResult = searchResult;
            } else {
                lastSearchResult = null;
                // Try to clean memory from old cashe
                runtime.gc();
                // If GC was executed and free memory amount increased, set new value to cashed last search result
                // else doesn't cashe it at all
                if(isSufficientMemoryAmount()) {
                    lastSearchResult = searchResult;
                }
            }
        }
        return searchResult;
    }

    /**
     * Get the results of the last search from a memory sensitive cache. The cache can
     * be cleared if the memory is needed and the search should just be performed again.
     * @return
     */
    public synchronized List<String> getLastResults() {
        if (this.lastSearchResult != null) {
            return this.lastSearchResult;
        } else {
            if(this.lastSearchPrefics == null) {
                return new ArrayList<String>();
            } else {
                return getWords(this.lastSearchPrefics);
            }
        }
    }

    /**
     * Checks if free memory size for this JVM is less than 20% of overall JVM memory size.
     *
     * @return true if free memory size for this JVM is more than 20% of overall
     * JVM memory size
     */
    private boolean isSufficientMemoryAmount() {
        return ((double)runtime.freeMemory() / (double)runtime.totalMemory()) > 0.2;
    }

    // Auxiliary class that gives warranty that the only one Dictionary instance is possible
    // in the multithread environment.
    public static class DictionarySingletonHolder {
        public static final Dictionary INSTANCE = new TreeDictionary();
    }
}
