package xpath;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.InputSource;

public class XPathTest 
{
	private static final String NAMESPACE_PREFFICS = "ns4";
	private static final String NAMESPACE_URI = "urn:expedia:om:common:defn:v1";
	private static final String XPATH_NS = "//ns4:TPID/text()";
	private static final String XPATH = "//TPID/text()";
	private static final String SITE_INFO_WITHOUT_NS_COUNT_XPATH = "boolean(count(//SiteInfo))";
	
	protected XPath m_xpath;

	public XPathTest()
	{
		initXPath();
	}
	
	public static void main(String[] args) throws Exception
	{
		InputStream fileWithoutNamespases = new FileInputStream(new File("OrderHeaderStaticAttributesOMS_v1.xml"));
		InputStream fileWithNamespases = new FileInputStream(new File("OrderHeaderStaticAttributesOMS_v1.1.xml"));
		
		
		XPathTest xpath = new XPathTest();
		
		System.out.println(xpath.executeXPath(fileWithoutNamespases, XPATH));
		System.out.println(xpath.executeXPath(fileWithNamespases, XPATH_NS));
		
//		System.out.println(xpath.executeXPath(fileWithoutNamespases, SITE_INFO_WITHOUT_NS_COUNT_XPATH));
//		System.out.println(xpath.executeXPath(fileWithNamespases, SITE_INFO_WITHOUT_NS_COUNT_XPATH));
//		
		fileWithoutNamespases.close();
		fileWithNamespases.close();
	}
	
	private void initXPath()
	{
		XPathFactory factory = XPathFactory.newInstance();
		m_xpath = factory.newXPath();
		m_xpath.setNamespaceContext(new NamespaceContext() 
		{
			
			@SuppressWarnings("rawtypes")
			public Iterator getPrefixes(String namespaceURI) 
			{
				// TODO Auto-generated method stub
				return null;
			}
			
			public String getPrefix(String namespaceURI) 
			{
				// TODO Auto-generated method stub
				return null;
			}
			
			public String getNamespaceURI(String prefix)
			{
				 return NAMESPACE_PREFFICS.equals(prefix) ?  NAMESPACE_URI : null;
			}
			
		});
	}
	
	protected String executeXPath(InputStream document, String xPathString) 
		throws XPathExpressionException
	{
		return m_xpath.evaluate(xPathString, new InputSource(document));
	}
	
}
