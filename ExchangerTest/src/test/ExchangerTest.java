package test;

import java.util.concurrent.Exchanger;

public class ExchangerTest {
	private static Exchanger<String> exchanger = new Exchanger<>();

	public static void main(String[] args) {
		new Thread(new John()).start();
		new Thread(new Bob()).start();
	}
	
	private static class John implements Runnable {
		@Override
		public void run() {
			try {
				System.out.println(exchanger.exchange("Hello Bob!!!"));
				System.out.println(exchanger.exchange("How are you, Bob?"));
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static class Bob implements Runnable {
		@Override
		public void run() {
			try {
				System.out.println(exchanger.exchange("Hello John!!!"));
				System.out.println(exchanger.exchange("I'm fine, John?"));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
