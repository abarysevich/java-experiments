package aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AnnotatedTestAspect 
{
	
	@Pointcut("execution(* test.beans.TestBean.get*(..))")
	public void pointcut(){}
	
	
	@AfterReturning(pointcut="pointcut()", returning="retVal")
	public void advice(Object retVal)
	{
		System.out.println("After returning advice has been triggered.");
		System.out.println(retVal.toString());
	}
	
}
