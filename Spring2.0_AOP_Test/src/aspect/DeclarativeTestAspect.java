package aspect;

public class DeclarativeTestAspect
{
	
	public void advice(Object retVal)
	{
		System.out.println("After returning advice has been triggered.");
		System.out.println(retVal.toString());
	}
	
}
