package test.beans;

import org.springframework.transaction.annotation.Transactional;

public class TestBean implements ITestBean
{
	private String property;
	
	
	public void setProperty(String property)
	{
		this.property = property;
	}
	
	public String getProperty()
	{
		return this.property;
	}
	
	public String getTestString()
	{
		return "firstTestString " + this.getProperty();
	}
	
	@Transactional
	public String getSecondTestString()
	{
		return "secondTestString " + this.getProperty();
	}
	
}
