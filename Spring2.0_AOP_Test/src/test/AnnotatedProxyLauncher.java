package test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import test.beans.ITestBean;


public class AnnotatedProxyLauncher 
{
	
	
	public static void main(String[] args) 
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("ioc-context-annotated.xml");
		ITestBean bean = context.getBean(ITestBean.class);
		
		System.out.println(bean.getTestString());
		System.out.println(bean.getSecondTestString());
	}
	
	
}
