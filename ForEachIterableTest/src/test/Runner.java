package test;

import java.util.Arrays;
import java.util.List;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		String[] array = "To be or not to be".split(" ");
		
		for(String element: array)
		{
			System.out.println(element);
		}
		
		List<String> list = Arrays.asList(array);  
		printUsingForEach(list);
		ReversedIteratorList<String> reversedList = new ReversedIteratorList<String>(list);
		printUsingForEach(reversedList);
		printUsingForEach(reversedList.reversed());
	}

	private static <T> void printUsingForEach(Iterable<T> collection)
	{
		for(T element: collection)
		{
			System.out.println(element);
		}
	}
	
}
