package test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ReversedIteratorList<T> extends ArrayList<T> 
{
	
	private static final long serialVersionUID = 1L;

	public ReversedIteratorList(Collection<T> collection)
	{
		super(collection);
	}

	public Iterable<T> reversed()
	{
		return new Iterable<T>() 
		{

			public Iterator<T> iterator() 
			{
				return new Iterator<T>() 
				{

					int currentIndex = size(); 
					
					public boolean hasNext() 
					{
						if(currentIndex > 0)
							return true;
						return false;
					}

					public T next() 
					{
						return get(--currentIndex);
					}

					public void remove() 
					{
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}
	
}
