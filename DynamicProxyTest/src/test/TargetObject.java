package test;

public class TargetObject implements ITargetObject 
{

	@Override
	public void interestingMethod() 
	{
		System.out.println("Interested object method was invoked");
	}

	@Override
	public void boringMethod() 
	{
		System.out.println("Boring object method was invoked");
	}	
	
}
