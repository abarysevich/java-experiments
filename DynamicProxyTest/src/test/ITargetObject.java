package test;

public interface ITargetObject 
{
	
	void interestingMethod();
	
	void boringMethod();
	
}
