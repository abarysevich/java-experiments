package test.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TargetObjectProxy<T> implements InvocationHandler 
{
	private T realObject;
	
	public TargetObjectProxy(T proxiedObject)
	{
		realObject = proxiedObject;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable 
	{
		System.out.println(proxy.getClass().getCanonicalName());
//		return method.invoke(realObject, args);
		return null;
	}

}
