package run;

import java.lang.reflect.Proxy;

import test.ITargetObject;
import test.TargetObject;
import test.proxy.TargetObjectProxy;


public class Runner 
{


	public static void main(String[] args) 
	{
		TargetObject obj = new TargetObject();
		
		ITargetObject targObject = (ITargetObject)Proxy.newProxyInstance(TargetObject.class.getClassLoader(),
				new Class<?>[]{ITargetObject.class}, new TargetObjectProxy<TargetObject>(obj));
		targObject.boringMethod();
	}

}
