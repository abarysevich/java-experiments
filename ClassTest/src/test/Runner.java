package test;

public class Runner {

	/**
	 * @param new args
	 */
	public static void main(String[] args) 
	{
		
		Candy can = new Candy();
		Class<?> clazz1 = can.getClass();
		
		
		Candy second = new Candy();
		Class<?> clazz2 = second.getClass();

		// Single Class object is used for all instances of that class.
		System.out.println(clazz2.equals(clazz1));
		
	}

}
