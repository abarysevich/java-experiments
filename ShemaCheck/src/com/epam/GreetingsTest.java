package com.epam;

public class GreetingsTest 
{
	public static void main(String[] args) 
	{
		GreetingsConstructor h = new GreetingsConstructor();
		h.make( "Bonjour, madame", "fr" ); 
		h.make( "Hey, you", "en" ); 
		h.marshal();
	}
}
