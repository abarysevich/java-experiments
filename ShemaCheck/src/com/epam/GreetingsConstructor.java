package com.epam;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import greetings.GreetingListType;
import greetings.GreetingType;
import greetings.ObjectFactory;

public class GreetingsConstructor 
{
	  private ObjectFactory of;
	  private GreetingListType grList;

	  public GreetingsConstructor()
	  {
	      of = new ObjectFactory();
	      grList = of.createGreetingListType();
	  }

	  public void make( String t, String l )
	  {
	      GreetingType g = of.createGreetingType();
	      g.setText( t );
	      g.setLanguage( l );
	      grList.getGreeting().add( g );
	  }

	    public void marshal() 
	    {
	    	try 
	    	{	
	            JAXBElement<GreetingListType> gl =
	                of.createGreetings( grList );
	            JAXBContext jc = JAXBContext.newInstance( "greetings" );
	            Marshaller m = jc.createMarshaller();
	            m.marshal( gl, System.out );
	        } 
	    	catch( JAXBException jbe )
	        {
	    		System.out.print(jbe.getLocalizedMessage());
	        }
	    }
}
