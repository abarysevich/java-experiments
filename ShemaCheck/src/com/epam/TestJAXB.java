package com.epam;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import schemafiles.ObjectFactory;
import schemafiles.Shiporder;
import schemafiles.Shiporder.Shipto;

public class TestJAXB 
{
	   private ObjectFactory objectFactory;
	   private Shiporder shipOrder;
	   
	   public TestJAXB()
	   {
		   this.objectFactory = new ObjectFactory();
		   this.shipOrder = this.objectFactory.createShiporder(); 
	   }
	   
	   public void make()
	   {
		   this.shipOrder.setOrderperson("Fedor");
		   
		   Shipto shipto = this.objectFactory.createShiporderShipto();
		   shipto.setCity("Minsk");
		   shipto.setCountry("Belarus");
		   shipto.setName("Misha");
		   
		   this.shipOrder.setShipto(shipto);
	   }
	   
	   public void marshall() throws Exception
	   {
		   JAXBContext context = JAXBContext.newInstance("schemafiles");
		   Marshaller marsh = context.createMarshaller();
		   marsh.marshal(shipOrder, System.out);
	   }
}
