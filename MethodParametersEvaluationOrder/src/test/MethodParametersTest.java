package test;

public class MethodParametersTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int first = 1;
		int second = 2;
		parametersPrinter(first, ++first);
		MethodParametersTest test = new MethodParametersTest();
		test.instanceParametersPrinter(second, --second);
	}

	private static void parametersPrinter(int first, int second) {
		System.out.printf("First: %d & Second: %d", first, second);
		System.out.println();
	}
	
	private void instanceParametersPrinter(int first, int second) {
		System.out.printf("First: %d & Second: %d", first, second);
		System.out.println();
	}
}
