package test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Test
{
	
	public <T> void genericInput(String test, T date)
	{
//		T.class call for class object is forbidden!!!!
		Class<?> clazz1 =  test.getClass();
		Class<T> clazz2 =  (Class<T>) test.getClass();
		Class<?> clazz3 = date.getClass();
		
		System.out.println(clazz1.getSimpleName());
		System.out.println(clazz3.getSimpleName());
		
		System.out.println(test);
		System.out.println(date);
		
	}
	
	public <L> List<L> genericReturn(String test)
	{
		System.out.println(test);
		return new ArrayList<L>();
	}
	
}
