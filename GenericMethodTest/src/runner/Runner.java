package runner;

import java.util.Date;
import java.util.List;

import test.Test;

public class Runner {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
		Test test = new Test();
		test.genericInput("generic", new Date());
	
//		Can't be compiled because treated as List<Object> - nowhere to retrieve type parameter:
//		inputList(test.genericReturn("test"));
		
//		Generic type is retrieved from returned parameter List<String> list  
		List<String> list = test.genericReturn("test");
		inputList(list);
	}
	
	private static void inputList(List<? extends String> list)
	{
		System.out.print(list.size());
	}
}
