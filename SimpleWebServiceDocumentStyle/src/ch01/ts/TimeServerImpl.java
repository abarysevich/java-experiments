package ch01.ts;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.DOCUMENT)
public class TimeServerImpl implements TimeServer 
{

	@WebMethod
	public String getTimeAsString() 
	{
		return new Date().toString();
	}

	@WebMethod
	public long getTimeAsElapsed() 
	{
		return new Date().getTime();
	}

}
