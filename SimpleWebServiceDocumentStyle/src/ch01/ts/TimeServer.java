package ch01.ts;


public interface TimeServer 
{
	
	String getTimeAsString();
	
	
	long getTimeAsElapsed();
		
}
