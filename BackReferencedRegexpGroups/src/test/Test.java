package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String patternString = "(\\d)(\\w)(\\d+)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher mat = pattern.matcher("1b92321343243254");
		
		if (mat.matches()) {
			System.out.print(mat.replaceAll("$3$2$1"));
		} else {
			System.out.print("Doesn't match!!!");
		}
	}

}
