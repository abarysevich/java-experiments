package com.test;

import java.util.function.Function;

public class Lambda {

    public static void main(String[] args) {
        // Simple functional interface usage example
        applyDoingStaff(()-> System.out.println("Hello from lambda!"));

        // Predefined functional interface usage
        System.out.println(applyFunction(s -> s.toLowerCase(), "String").toString());

        // Static method reference usage
        System.out.println(applyFunction(Integer::bitCount, 22));
    }

    @FunctionalInterface
    public interface Functional {
        void doStuff();
    }

    private static void applyDoingStaff(Functional doer) {
        doer.doStuff();
    }

    private static <T, R> R applyFunction(Function<T,R> function, T parameter) {
        return function.apply(parameter);
    }
}
