package com.test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Streams {
    private static final String DICTIONARY_FILE = "words.txt";

    public static void main(String[] args) throws IOException {
        List<String> words = new Streams().readWords(DICTIONARY_FILE);

        Map<String, List<String>> sameLetterWords = getSameLettersWords(words);

        sameLetterWords.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 5)
                .forEach(System.out::println);

        Long totalWords = sameLetterWords.entrySet().stream()
                .map(entry -> entry.getValue())
                .flatMap(wordsList -> wordsList.stream()).collect(Collectors.counting());

        System.out.println();
        System.out.printf("Total words count: %d", totalWords);

        List<Integer> integers = getRandomIntegers(10000000, 1000);
        int maxInt = integers.stream().max(Integer::compare).orElseThrow(IllegalArgumentException::new);
        System.out.printf("Max int: %d", maxInt);

        System.out.println();

        double avg = integers.stream().collect(Collectors.averagingInt(i -> i.intValue()));
        System.out.printf("Average : %.3f", avg);

        System.out.println();

        Map<Integer, Long> numbersCount =
                integers.stream().collect(
                        Collectors.groupingBy(i -> i.intValue(), Collectors.counting()));

        numbersCount.entrySet().stream().filter(entry -> entry.getValue() > 3).forEach(System.out::println);

        long startTimeMills = GregorianCalendar.getInstance().getTimeInMillis();

        double numbersSqrtSum = integers.stream().parallel()
                .reduce(1.0, (result, number)-> result + Math.sqrt(number),
                        (firstPartialResult, secondPartialResult) -> firstPartialResult + secondPartialResult);

        long endTimeMills = GregorianCalendar.getInstance().getTimeInMillis();

        System.out.printf("Time mills : %d", endTimeMills - startTimeMills);

        System.out.println();

        System.out.printf("Numbers square roots sum : %.4f", numbersSqrtSum);

        System.out.println();
    }

    private static Map<String, List<String>> getSameLettersWords(List<String> words) {
        return words.stream().collect(
                Collectors.groupingBy(word -> alphabetise(word), Collectors.toList()));
    }


    private static List<Integer> getRandomIntegers(int size, int bound) {
        List<Integer> result = new ArrayList<>(size);
        Random rand = new Random();
        while(size-- > 0) {
            result.add(rand.nextInt(bound));
        }
        return result;
    }

    private static String alphabetise(String word) {
        char[] characters = word.toCharArray();
        Arrays.sort(characters);
        return new String(characters);
    }

    private List<String> readWords(String filename) throws IOException {
        List<String> result = new ArrayList<>();
        try (Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(filename))) {
            while(scanner.hasNextLine()) {
                result.add(scanner.nextLine().trim());
            }
        }
        return result;
    }
}
