package publisher;

import javax.xml.ws.Endpoint;

import ch01.ts.TimeServerImpl;



public class Launcher 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
		 Endpoint.publish(
			      "http://localhost:9876/ts", new TimeServerImpl());
	}

}
