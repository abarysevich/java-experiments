package test;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;



public class HTTPTest {

	
	// see this http://stackoverflow.com/questions/2793150/how-to-use-java-net-urlconnection-to-fire-and-handle-http-requests
	
	public static void main(String[] args) throws Exception 
	{
		URL url = new URL("http://127.0.0.1:9876/ts");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
//		connection.setRequestProperty("Accept-Charset", "UTF-8");
		connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		connection.setRequestProperty("Content-Length", "210");
		
		OutputStream out = connection.getOutputStream();
		out.write("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ts=\"http://ts.ch01/\"> <soapenv:Body><ts:getTimeAsElapsed/></soapenv:Body></soapenv:Envelope>".getBytes("UTF-8"));
		out.close();
		
		connection.connect();
		System.out.print(connection.getResponseMessage());
		
		Scanner scan = new Scanner(connection.getInputStream());
		while(scan.hasNextLine())
		{
			System.out.print(scan.nextLine());
		}
		scan.close();
	}

	
}
